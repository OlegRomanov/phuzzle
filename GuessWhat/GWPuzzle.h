//
//  GWPuzzle.h
//  Phuzzle
//
//  Created by Artem Antipov on 27.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GWProfile, GWQuestion;

@interface GWPuzzle : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSNumber * coins;
@property (nonatomic, retain) NSNumber * correct;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSNumber * keySolved;
@property (nonatomic, retain) NSNumber * my;
@property (nonatomic, retain) NSString * order;
@property (nonatomic, retain) NSNumber * resolved;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * isStarted;
@property (nonatomic, retain) GWProfile *opponent;
@property (nonatomic, retain) NSSet *questions;
@end

@interface GWPuzzle (CoreDataGeneratedAccessors)

- (void)addQuestionsObject:(GWQuestion *)value;
- (void)removeQuestionsObject:(GWQuestion *)value;
- (void)addQuestions:(NSSet *)values;
- (void)removeQuestions:(NSSet *)values;

@end
