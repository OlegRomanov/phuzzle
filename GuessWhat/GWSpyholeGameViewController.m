//
//  GWSpyholeGameViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWSpyholeGameViewController.h"

#import "GWAnswerView.h"
#import "GWGameTimerView.h"
#import "GWGameAlertView.h"

#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

#import "GWBlurFilterMask.h"

#import <CoreMotion/CoreMotion.h>

#import <sys/utsname.h>

@interface GWSpyholeGameViewController ()
{
    CMMotionManager *_motionManager;
}

@property (strong, nonatomic) IBOutlet UIView *timerContainerView;
@property (strong, nonatomic) IBOutlet UIView *controllContainerView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;


@property (strong) GWBlurFilterMask *blurFilterMask;

@property (strong, nonatomic) GWGameTimerView * timerView;

@end

@implementation GWSpyholeGameViewController

NSString* machineName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (!self.puzzle) {
        self.puzzle = [GWPuzzle MR_createEntity];
        self.puzzle.type = kPuzzleTypeSpyhole;
        self.puzzle.answer = @"Kangaroo";
        self.puzzle.opponent = [GWProfile createEntryForID:@2];
        self.puzzle.my = @NO;
        self.puzzle.imageURL = @"http://s21.postimg.org/jqaqeixpj/KANGA.jpg";
        [self loadImage];
    }
    
    
    [self.controllContainerView addSubview:self.answerView];
    [self.timerContainerView addSubview:self.timerView];
    
    self.imageView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0.302 alpha:1.0].CGColor;
    self.imageView.layer.borderWidth = 2.0;
    self.imageView.layer.cornerRadius = 15.0;

    if (![NSUserDefaults standardUserDefaults].spyholeHelp && !self.demoVersion) {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Tilt your device to move the spyhole and type your guess below. Good luck!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Don't Show Again", nil] show];
    }
    
    NSTimeInterval addTime = [NSUserDefaults standardUserDefaults].seconds ? 20 : 0;
    
    self.timerView.timerLabel.text = [NSString stringWithFormat:@"%@", @(TIMER_SPYHOLE_START_COUNT+addTime)];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag != 999) {
        [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    }
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [NSUserDefaults standardUserDefaults].spyholeHelp = YES;
            break;
            
        default:
            break;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_motionManager stopGyroUpdates];
}



-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y -= size.height;
        self.controllContainerView.frame = frame;
    } completion:nil];
    
}

-(void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y += size.height;
        self.controllContainerView.frame = frame;
    } completion:nil];
    
}

-(void)imageDidLoad:(UIImage *)image
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.image = image;
        self.imageView.image = image;
        [self beginBlurMasking];
        if (!self.demoVersion) {
            [self.timerView startTimer:TIMER_SPYHOLE_START_COUNT];
        }
        [self setupMotionManager];
    });
}


- (void)gameDidEnd:(BOOL)correct
{
    [_motionManager stopGyroUpdates];
}


#pragma mark - spyhole

- (void)beginBlurMasking
{
    GWBlurFilterMask *blurFilterMask = [GWBlurFilterMask layer];
    blurFilterMask.diameter = 15;
    blurFilterMask.frame = self.imageView.bounds;
    blurFilterMask.origin = self.imageView.center;
    blurFilterMask.shouldRasterize = YES;
    [self.imageView.layer addSublayer:blurFilterMask];
    [blurFilterMask setNeedsDisplay];
    
    self.blurFilterMask = blurFilterMask;
}


- (void) setupMotionManager
{
    //    [self startUpdates];
    _motionManager = [[CMMotionManager alloc] init];
    _motionManager.gyroUpdateInterval = 1.0/40.0; // Update every 1/2 second.
    
    int delta;
    if ([machineName() isEqualToString:@"iPhone3,1"] || [machineName() isEqualToString:@"iPhone4,1"] ){
        delta = 12;
    } else if ([machineName() isEqualToString:@"iPhone5,1"] || [machineName() isEqualToString:@"iPhone5,2"] ){
        delta = 9;
    } else {
        delta = 10;
    }
    
    if (_motionManager.gyroAvailable) {
        NSOperationQueue *queue = [NSOperationQueue currentQueue];
        [_motionManager startGyroUpdatesToQueue:queue
                                    withHandler: ^ (CMGyroData *gyroData, NSError *error) {
                                        CMRotationRate rotate = gyroData.rotationRate;
                                        
                                        CGFloat x = roundf(rotate.x*10)/10;
                                        CGFloat y = roundf(rotate.y*10)/10;
                                        NSLog(@"x=%f y=%f", x, y);
                                        
                                        
                                        CGPoint point = self.blurFilterMask.origin;
                                        point.x += delta*(y);
                                        point.y += delta*(x);
                                        
                                        CGRect frame = [self imageRect];
                                        
                                        if (point.x < frame.origin.x) {
                                            point.x =frame.origin.x;
                                        } else if (point.x > frame.size.width+frame.origin.x){
                                            point.x = frame.size.width+frame.origin.x;
                                        }
                                        if (point.y < frame.origin.y) {
                                            point.y =frame.origin.y;
                                        } else if (point.y > frame.size.height+frame.origin.y){
                                            point.y = frame.size.height+frame.origin.y;
                                        }
                                        self.blurFilterMask.origin = point;
                                        //                                            [self.blurFilterMask setNeedsDisplay];
                                        
                                        [self.blurFilterMask performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:self.blurFilterMask waitUntilDone:NO];
                                        
                                    }];
        
        
    }
}

- (CGRect) imageRect
{
    CGFloat rationX = self.image.size.width/self.image.size.height;
    CGFloat rationY = self.image.size.height/self.image.size.width;
    
    CGFloat width = self.imageView.frame.size.width ;
    CGFloat height = self.imageView.frame.size.height;
    
    if (rationY < rationX) {
        height = self.imageView.frame.size.width * rationY;
    } else{
        width = self.imageView.frame.size.height * rationX;
    }
    
    CGRect frame = CGRectMake((self.imageView.frame.size.width-width)/2, (self.imageView.frame.size.height-height)/2, width, height);
    return frame;
}





@end
