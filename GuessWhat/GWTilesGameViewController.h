//
//  GWTilesGameViewController.h
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"
#import "GWBaseGameViewController.h"

@class GWPuzzle;
@class GWProfile;

@interface GWTilesGameViewController : GWBaseGameViewController


@end
