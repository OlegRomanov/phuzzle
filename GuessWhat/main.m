//
//  main.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GWAppDelegate.h"
#import "GWApplication.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([GWApplication class]), NSStringFromClass([GWAppDelegate class]));
    }
}
