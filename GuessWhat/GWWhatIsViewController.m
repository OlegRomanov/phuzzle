//
//  GWWhatIsViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWWhatIsViewController.h"

@interface GWWhatIsViewController ()
<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITextField *whatIsField;

@end

@implementation GWWhatIsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.whatIsField.delegate = self;
    self.imageView.image = self.image;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.whatIsField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
}

- (IBAction)okPressed:(UIButton *)sender {
    if (![self validate]) {
        return;
    }
    if (self.delegate) {
         NSString *str = [self.whatIsField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self.delegate didCapturedImage:self.image withName:str toProfile:self.profile];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (IBAction)backPressed:(id)sender {
//    if (self.delegate) {
//        [self.delegate captureImageWillDissmis];
//    }
//    [self dismissViewControllerAnimated:YES completion:^{
//        if (self.delegate) {
//            [self.delegate captureImageDidDissmis];
//        }
//    }];
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL) validate
{
    NSString *str = [self.whatIsField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!str || str.length <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:@"You need to tell the other player the name of the Phuzzle." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (result.length  > 25) {
        return NO;
    }
    
    return YES;
}

@end
