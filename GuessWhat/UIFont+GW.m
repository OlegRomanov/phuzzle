//
//  UIFont+GW.m
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "UIFont+GW.h"

@implementation UIFont (GW)

+ (UIFont*)defaultFontWithSize:(CGFloat) size
{
    NSArray* fonts = [UIFont fontNamesForFamilyName:@"Komika Title"];
    if (fonts.count == 0) {
        return [UIFont italicSystemFontOfSize:size];
    }
    NSString *font = fonts[0];
    return [UIFont fontWithName:font size:size];
}

@end
