//
//  IAHelper.m
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "IAPHelper.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductRestoredNotification = @"IAPHelperProductRestoredNotification";
NSString *const IAPHelperFailedTransactionNotification = @"IAPHelperFailedTransactionNotification";

@interface IAPHelper()
<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    SKProductsRequest * _productsRequest;
    RequestProductsCompletionHandler _completionHandler;
    NSSet * _productIdentifiers;
}



@end

@implementation IAPHelper

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
        
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        // Сохраняем идентификаторы продуктов
        _productIdentifiers = productIdentifiers;
        
    }
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    NSMutableSet *set = [NSMutableSet setWithSet:_productIdentifiers];
    [set addObject:[NSString stringWithFormat:@"iap_id"]];
    
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

- (void) restorePurchased
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark - SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Загруженый список продуктов...");
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Найден продукт: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Не смог загрузить список продуктов.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}


- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Покупаем %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self completeTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");

    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:transaction userInfo:nil];
    
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductRestoredNotification object:transaction userInfo:nil];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperFailedTransactionNotification object:transaction userInfo:nil];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) finishTransaction:(SKPaymentTransaction*) transaction
{
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

@end
