//
//  GWMainViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWMainViewController.h"
#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"

#import "GWLoginViewController.h"

#import "GWTilesGameViewController.h"
#import "GWSpyholeGameViewController.h"
#import "GWQuizGameViewController.h"

@interface GWMainViewController ()
<GKGameCenterControllerDelegate, AppiraterDelegate>
{
    NSArray *_products;
}
@property (strong, nonatomic) IBOutlet UIButton *buySecondsButton;

@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) NSArray *leaderboards;
@property (strong, nonatomic) IBOutlet UIImageView *keysImage;



@end

@implementation GWMainViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = NO;
    self.bgImageName = @"BACKGROUND5";
    self.noADS = YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.logoutButton.sound = @"UserLoginLogout";
    [self looseAllStartedPuzzles];
    
    
    
    [self authenticateLocalPlayer];
    
    if (![NSUserDefaults standardUserDefaults].review){
        [Appirater setAppId:@"819213078"];
        [Appirater setDaysUntilPrompt:7];
        [Appirater setUsesUntilPrompt:5];
        [Appirater setSignificantEventsUntilPrompt:-1];
        [Appirater setTimeBeforeReminding:2];
        [Appirater setDebug:NO];
        [Appirater appLaunched:YES];
        [Appirater setDelegate:self];
    }
    
    if ([NSUserDefaults standardUserDefaults].seconds) {
        self.buySecondsButton.hidden = YES;
    } else {
        self.buySecondsButton.enabled = NO;
        [[GWStoreHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                _products = products;
                if (products.count > 0) {
                    self.buySecondsButton.enabled = YES;
                } else {
                    self.buySecondsButton.enabled = NO;
                }
            }
        }];
    }
    
    
    //    if (DEBUG) {
    //        [[GWDataLoader sharedInstance] getKeys:@50 WithCompletionBlock:^(id error, id result) {
    //        }];
    //    }
}

-(void)appiraterDidOptToRate:(Appirater *)appirater
{
    [[GWDataLoader sharedInstance] getKeyWithCompletionBlock:^(id error, id result) {
        [NSUserDefaults standardUserDefaults].review = YES;
    }];
}


- (void) looseAllStartedPuzzles
{
    NSArray *puzzles = [GWPuzzle MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"isStarted == YES && resolved = NO"]];
    NSLog(@"looseAllStartedPuzzles: %@", puzzles);
    
    for (GWPuzzle * puzzle in puzzles) {
        
        puzzle.resolved = @YES;
        puzzle.correct = @NO;
        puzzle.coins = @0;
        
        [[GWDataLoader sharedInstance] sendPuzzleRezult:puzzle andCompletinoBlock:^(id error, id result) {}];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.navigationController setViewControllers:@[self] animated:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionFailed:) name:IAPHelperFailedTransactionNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(achivesLoaded) name:GWGameCenterAchiveLoadedNotification object:nil];
    
    [self achivesLoaded];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperFailedTransactionNotification object:nil];
}


#pragma mark - Game Center

- (void) achivesLoaded
{
    GKAchievement *keys = [[GWGameCenterHelper sharedInstance] getAchievementForIdentifier:GWAchiviment100Keys];
    if (keys.isCompleted) {
        //TODO ACHIVE KEYS image for comleted
        self.keysImage.image = [UIImage imageNamed:@"100YES"];
    } else {
        //TODO ACHIVE KEYS image for not completed
        self.keysImage.image = [UIImage imageNamed:@"100NO"];
    }
}

- (IBAction)gameCenterPressed:(id)sender {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if (localPlayer.isAuthenticated) {
        [self showLeaderboard:@"grp.com.Phuzzle.coninsLeaderboard"];
    } else {
        [self authenticateLocalPlayer];
    }
}

- (void) authenticateLocalPlayer
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil)
        {
            //showAuthenticationDialogWhenReasonable: is an example method name. Create your own method that displays an authentication view when appropriate for your app.
            //            [self showAuthenticationDialogWhenReasonable: viewController];
            
            [self presentViewController:viewController animated:YES completion:nil];
            NSLog(@"auth hz");
        } else if (localPlayer.isAuthenticated)
        {
            //authenticatedPlayer: is an example method name. Create your own method that is called after the loacal player is authenticated.
            //            [self authenticatedPlayer: localPlayer];
            NSLog(@"auth ok");
            
            [[GWGameCenterHelper sharedInstance] loadAchievements];
        }
        else
        {
            NSLog(@"auth bad: %@", error);
            //            [self disableGameCenter];
        }
    };
}

- (void) showLeaderboard: (NSString*) leaderboardID
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        gameCenterController.viewState = GKGameCenterViewControllerStateAchievements;
        [self presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)buySecondsPressed:(id)sender {
    self.buySecondsButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    SKProduct *product = _products[0];
    for (SKProduct *prod in _products) {
        if ([prod.productIdentifier isEqualToString:@"com.Whatthephuzzle.seconds30forever"]) {
            product = prod;
            break;
        }
    }
    
    NSLog(@"Покупаем %@...", product.productIdentifier);
    [[GWStoreHelper sharedInstance] buyProduct:product];
}

- (IBAction)restorePressed:(id)sender {
    
    [[GWStoreHelper sharedInstance] restorePurchased];
    
}

- (IBAction)logoutPressed:(id)sender {
    [[GWDataLoader sharedInstance] logout];
    
    GWLoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWLoginViewController"];
    [self.navigationController setViewControllers:@[vc] animated:YES];
}

- (void)productPurchased:(NSNotification *)notification {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    SKPaymentTransaction * transaction = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:transaction.payment.productIdentifier]) {
            if ([@"com.Whatthephuzzle.keys3" isEqualToString:transaction.payment.productIdentifier]) {
            } else if ([@"com.Whatthephuzzle.seconds30forever" isEqualToString:transaction.payment.productIdentifier]) {
                self.buySecondsButton.hidden = YES;
            }
            
        }
    }];
}

- (void) transactionFailed:(NSNotification*) notification
{
    self.buySecondsButton.enabled = YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


@end
