//
//  GWPuzzleSendViewController.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"
@class GWProfile;

@interface GWPuzzleSendViewController : GWBaseViewController

@property (nonatomic, strong) GWProfile *profile;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *whatIs;

@end
