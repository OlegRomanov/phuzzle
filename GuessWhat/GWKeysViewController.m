//
//  GWKeysViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 24.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWKeysViewController.h"
#import "GWProgressBar.h"

#import "GWProfile+Methods.h"
@interface GWKeysViewController ()
{
    NSArray *_products;
}
@property (strong, nonatomic) IBOutlet UIButton *reviewButton;
@property (strong, nonatomic) IBOutlet UIButton *buyButton;
@property (strong, nonatomic) IBOutlet UIButton *secondsButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *adClicksPBC;
@property (strong, nonatomic) IBOutlet UIView *referalsPBC;
@property (strong, nonatomic) IBOutlet UIView *winsPBS;
@property (strong, nonatomic) IBOutlet UIView *coinsPBC;
@property (strong, nonatomic) IBOutlet UIView *reviewPBC;

@property (strong, nonatomic) IBOutlet UILabel *refLabel;
@property (strong, nonatomic) IBOutlet UILabel *winsLabel;
@property (strong, nonatomic) IBOutlet UILabel *coinsLabel;
@property (strong, nonatomic) IBOutlet UILabel *adLabel;
@property (strong, nonatomic) IBOutlet UILabel *reviewLabel;
@end

@implementation GWKeysViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = NO;
    self.bgImageName = @"BACKGROUND5";
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.buyButton.enabled = NO;
    self.secondsButton.enabled = NO;
    
    if ([NSUserDefaults standardUserDefaults].review) {
        self.reviewButton.hidden = YES;
    }
    
    if ([NSUserDefaults standardUserDefaults].seconds) {
        self.secondsButton.hidden = YES;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GWStoreHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
            if (products.count > 0) {
                self.buyButton.enabled = YES;
                self.secondsButton.enabled = YES;
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Nothing that you can buy." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
    GWProfile *profile = [GWProfile currentUser];
    
    {
        self.adLabel.text = [NSString stringWithFormat:@"AD CLICK COUNT (%@/100)", profile.adClicks];
        GWProgressBar *progressBar = [GWProgressBar viewWithProgress:profile.adClicks.integerValue/100.0];
        progressBar.frame = self.adClicksPBC.bounds;
        [self.adClicksPBC addSubview:progressBar];
    }
    
    {
        self.refLabel.text = [NSString stringWithFormat:@"REFERRALS (%@/3)", profile.referrals];
        GWProgressBar *progressBar = [GWProgressBar viewWithProgress:profile.referrals.integerValue/3.0];
        progressBar.frame = self.referalsPBC.bounds;
        [self.referalsPBC addSubview:progressBar];
    }
    
    {
        self.winsLabel.text = [NSString stringWithFormat:@"HIGHEST WINNING STREAK (%@/20)", profile.higestConsecutiveWins];
        GWProgressBar *progressBar = [GWProgressBar viewWithProgress:profile.higestConsecutiveWins.integerValue/20.0];
        progressBar.frame = self.winsPBS.bounds;
        [self.winsPBS addSubview:progressBar];
    }
    
    {
        NSInteger coins = profile.coins.integerValue - ((NSInteger)((NSInteger)(profile.coins.integerValue/100))*100.);
        self.coinsLabel.text = [NSString stringWithFormat:@"COINS (%@/100)", @(coins)];
        GWProgressBar *progressBar = [GWProgressBar viewWithProgress:coins/100.0];
        progressBar.frame = self.coinsPBC.bounds;
        [self.coinsPBC addSubview:progressBar];
    }
    
    {        
        self.reviewButton.hidden = [NSUserDefaults standardUserDefaults].review;
        NSNumber *review = [NSUserDefaults standardUserDefaults].review ? @1 : @0;
        self.reviewLabel.text = [NSString stringWithFormat:@"RATE APP (%@/1)", review];
        GWProgressBar *progressBar = [GWProgressBar viewWithProgress:review.floatValue];
        progressBar.frame = self.reviewPBC.bounds;
        [self.reviewPBC addSubview:progressBar];
    }
    
	// Do any additional setup after loading the view.
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = CGSizeMake(304, 420);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)buyPressed:(id)sender {
    self.buyButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    SKProduct *product = _products[0];
    for (SKProduct *prod in _products) {
        if ([prod.productIdentifier isEqualToString:@"com.Whatthephuzzle.keys3"]) {
            product = prod;
            break;
        }
    }
    
    NSLog(@"Покупаем %@...", product.productIdentifier);
    [[GWStoreHelper sharedInstance] buyProduct:product];
}
- (IBAction)restorePressed:(id)sender {
    
    [[GWStoreHelper sharedInstance] restorePurchased];
    [[[UIAlertView alloc] initWithTitle:nil message:@"Your Purchased Items Are Being Restored" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}
- (IBAction)buySecondsPressed:(id)sender {
    self.secondsButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    SKProduct *product = _products[0];
    for (SKProduct *prod in _products) {
        if ([prod.productIdentifier isEqualToString:@"com.Whatthephuzzle.seconds30forever"]) {
            product = prod;
            break;
        }
    }
    
    NSLog(@"Покупаем %@...", product.productIdentifier);
    [[GWStoreHelper sharedInstance] buyProduct:product];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionFailed:) name:IAPHelperFailedTransactionNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)productPurchased:(NSNotification *)notification {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    SKPaymentTransaction * transaction = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:transaction.payment.productIdentifier]) {
            if ([@"com.Whatthephuzzle.keys3" isEqualToString:transaction.payment.productIdentifier]) {
                self.buyButton.enabled = YES;
                [[[GWAudioPlayer alloc] initWithSound:@"EarnKey"]playIfSoundisEnabled];
                
                UIView * view = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
                view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
                view.alpha = 0.0;
                [self.navigationController.view addSubview:view];
                
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
                image.center = view.center;
                
                //TODO KEYS change image
                image.image = [UIImage imageNamed:@"BUY3KEYSMESSAGE"];
                image.userInteractionEnabled = NO;
                [view addSubview:image];
                
                [UIView animateWithDuration:0.20 animations:^{
                    view.alpha = 1.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.25 delay:1 options:0 animations:^{
                        view.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [view removeFromSuperview];
                    }];
                }];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(dismissPopup:)];
                
                [view addGestureRecognizer:tap];
            } else if ([@"com.Whatthephuzzle.seconds30forever" isEqualToString:transaction.payment.productIdentifier]) {
                self.secondsButton.hidden = YES;
            }
            
        }
    }];
}

- (void) transactionFailed:(NSNotification*) notification
{
    self.buyButton.enabled = YES;
    self.secondsButton.enabled = YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) dismissPopup:(UITapGestureRecognizer*) tap
{
    UIView *view = tap.view;
    
    [UIView animateWithDuration:0.15 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

- (IBAction)reviewPressed:(id)sender {
    [Appirater rateApp];
    
    [[GWDataLoader sharedInstance] getKeyWithCompletionBlock:^(id error, id result) {
        [NSUserDefaults standardUserDefaults].review = YES;
        [sender setHidden:YES];
    }];
}



@end
