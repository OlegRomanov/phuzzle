//
//  GWGameCenterHelper.m
//  Phuzzle
//
//  Created by Artem Antipov on 10.02.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWGameCenterHelper.h"

NSString *const GWAchiviment100Keys = @"com.Phuzzle.achive.keys100";

NSString *const GWGameCenterAchiveLoadedNotification = @"GWGameCenterAchiveLoadedNotification";

@interface GWGameCenterHelper ()

@property(nonatomic, retain) NSMutableDictionary *achievementsDictionary;

@end

@implementation GWGameCenterHelper

+ (GWGameCenterHelper *)sharedInstance
{
    static dispatch_once_t once;
    static GWGameCenterHelper * sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


- (id)init
{
    self = [super init];
    if (self) {
        self.achievementsDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void) loadAchievements
{
    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error)
     {
         if (error == nil)
         {
             for (GKAchievement* achievement in achievements){
                 [self.achievementsDictionary setObject: achievement forKey: achievement.identifier];
             }
             NSLog(@"%@", achievements);
             [[NSNotificationCenter defaultCenter] postNotificationName:GWGameCenterAchiveLoadedNotification object:nil];
         }
     }];
}

- (GKAchievement*) getAchievementForIdentifier: (NSString*) identifier
{
    GKAchievement *achievement = [self.achievementsDictionary objectForKey:identifier];
    if (achievement == nil)
    {
        achievement = [[GKAchievement alloc] initWithIdentifier:identifier];
        [self.achievementsDictionary setObject:achievement forKey:achievement.identifier];
    }
    return achievement;
}

- (void) reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent
{
    GKAchievement *achievement = [self getAchievementForIdentifier:identifier];
    if (achievement)
    {
        achievement.percentComplete = percent;
        [achievement reportAchievementWithCompletionHandler:^(NSError *error)
         {
             if (error != nil)
             {
                 // Log the error.
             }
         }];
    }
}

@end
