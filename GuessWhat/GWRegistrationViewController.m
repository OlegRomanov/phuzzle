//
//  GWRegistrationViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWRegistrationViewController.h"
#import "GWMainViewController.h"

@interface GWRegistrationViewController ()
<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet GWTextField *usernameField;
@property (strong, nonatomic) IBOutlet GWTextField *emailField;
@property (strong, nonatomic) IBOutlet GWTextField *passwordField;
@property (strong, nonatomic) IBOutlet GWTextField *confirmPasswordField;

@end

@implementation GWRegistrationViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.noADS = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.usernameField becomeFirstResponder];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = self.scrollView.frame.size;
}

-(void)keyboardWillShow:(NSNotification*)notification {
//    NSDictionary *userInfo = [notification userInfo];
//    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
//    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    double duration = [number doubleValue];
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom + 100, self.scrollView.contentInset.right)];
}

-(void)keyboardWillHide:(NSNotification*)notification {
//    NSDictionary *userInfo = [notification userInfo];
//    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//    
//    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    double duration = [number doubleValue];
    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom - 100 , self.scrollView.contentInset.right)];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameField) {
        [self.emailField becomeFirstResponder];
    } else if(textField == self.emailField){
        [self.passwordField becomeFirstResponder];
    } else if(textField == self.passwordField){
        [self.confirmPasswordField becomeFirstResponder];
    } else if(textField == self.confirmPasswordField){
        [self registerPressed:textField];
    }
    return NO;
}

- (IBAction)registerPressed:(id)sender {
    if (![self validate]) {
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GWDataLoader sharedInstance] registerWithUsername:self.usernameField.text withPassword:self.passwordField.text withEmail:self.emailField.text andCompletinoBlock:^(id error, id result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
//            GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
//            [self.navigationController pushViewController:vc animated:YES];
            [[[UIAlertView alloc] initWithTitle:nil message:@"Success! Let's Get Phuzzled!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//            [[GWDataLoader sharedInstance] logout];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL) validate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    if (self.usernameField.text.length == 0) {
        alert.title = @"Please Enter Your Username";
        [alert show];
        return NO;
    }
    if (self.emailField.text.length == 0) {
        alert.title = @"Please Enter Your Email Address";
        [alert show];
        return NO;
    }
    if (self.passwordField.text.length == 0) {
        alert.title = @"Please Enter Your Password";
        [alert show];
        return NO;
    }
    if (![self.confirmPasswordField.text isEqualToString:self.passwordField.text]) {
        alert.title = @"Passwords Do Not Match";
        [alert show];
        return NO;
    }
    
    return YES;
}

@end
