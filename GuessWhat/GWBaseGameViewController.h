//
//  GWBaseGameViewController.h
//  Phuzzle
//
//  Created by Artem Antipov on 27.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"
#import "CustomIOS7AlertView.h"
@class GWPuzzle;
@class GWProfile;
@class GWGameTimerView;
@class GWAnswerView;

//
//@protocol GWBaseGameViewControllerDelegate <NSObject>
//@required
//- (void) playPuzzle;
//@end

@interface GWBaseGameViewController : GWBaseViewController
<UIAlertViewDelegate,CustomIOS7AlertViewDelegate>
@property (strong, nonatomic) GWGameTimerView * timerView;
@property (strong, nonatomic) GWAnswerView * answerView;

@property (strong, nonatomic) UIImage *image;
@property (nonatomic, strong) GWPuzzle *puzzle;
@property (nonatomic, strong) GWProfile *profile;
@property (nonatomic, strong) UIImage *imageCorrect;
@property (nonatomic, readwrite) BOOL demoVersion;

- (void) loadImage;
- (void) imageDidLoad:(UIImage*) image;

-(void)keyboardWillHide:(NSNotification*)notification;
-(void)keyboardWillShow:(NSNotification*)notification;

- (void) startNewGame;

- (void) gameDidEnd:(BOOL) correct;
- (void) gameWin;
- (void) gameLoose;

@end
