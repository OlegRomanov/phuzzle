//
//  GWQuizGameViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWQuizGameViewController.h"

#import "GWAnswerView.h"
#import "GWGameTimerView.h"

#import "GWGameAlertView.h"

#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWPuzzle+Methods.h"
#import "GWQuestion+Methods.h"
#import "GWProfile+Methods.h"

#import "GWQuestionsView.h"



@interface GWQuizGameViewController ()
<GWQuestionsViewDelegate, UIAlertViewDelegate>
{
    NSInteger _rightAnswers;
    NSInteger _questIndex;
}

@property (strong,nonatomic) UIImage *image;

@property (strong,nonatomic) NSMutableArray *questions;

@property (strong, nonatomic) IBOutlet UIView *quasdsContainer;
@property (strong, nonatomic) IBOutlet UIView *timerContainerView;
@property (strong, nonatomic) IBOutlet UIView *controllContainerView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIView *questionsContainerView;
@property (strong, nonatomic) GWGameTimerView * timerView;

@end

@implementation GWQuizGameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (!self.puzzle) {
        self.puzzle = [GWPuzzle MR_createEntity];
        self.puzzle.type = kPuzzleTypeQuiz;
        self.puzzle.answer = @"Kangaroo";
        self.puzzle.opponent = [GWProfile createEntryForID:@2];
        self.puzzle.my = @NO;
        self.puzzle.imageURL = @"http://s21.postimg.org/jqaqeixpj/KANGA.jpg";
        [self loadImage];
    }
    
    if (self.puzzle.questions.count == 0) {
        
        if (!self.demoVersion) {
            [[GWDataLoader sharedInstance] loadQuestoinsWithCompletionBlock:^(id error, id result) {
                if (!error) {
                    [self.puzzle addQuestions:[NSSet setWithArray:result]];
                    self.questions = [[self.puzzle.questions allObjects] mutableCopy];
                    _questIndex = 0;
                    [self loadImage];
                }
            }];
        } else {
            
            //TODO QUIZ preview questions
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 1?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 2?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 3?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 4?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 5?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 6?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 7?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 8?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 9?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 10?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 11?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 12?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 13?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 14?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 15?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
            }
            {
                GWQuestion *question = [GWQuestion MR_createEntity];
                
                question.question = [NSString stringWithFormat:@"Preview Question 16?"];
                question.answers = @"Incorrect,Correct,Incorrect";
                question.correctAnswer = @"Correct";
                
                [self.puzzle addQuestionsObject:question];
        }
        }
    }else{
        [self loadImage];
    }
    
    self.questions = [[self.puzzle.questions allObjects] mutableCopy];
    _questIndex = 0;
    
    
    [self.controllContainerView addSubview:self.answerView];
    
    
    [self.timerContainerView addSubview:self.timerView];
    
    self.imageView.layer.borderColor = [UIColor blackColor].CGColor;
    self.imageView.layer.borderWidth = 4.0;
    self.imageView.layer.cornerRadius = 5.0;
    self.imageView.layer.backgroundColor = [UIColor blackColor].CGColor;
    self.imageView.layer.opacity = 1;
    
    self.quasdsContainer.layer.borderColor = [UIColor blackColor].CGColor;
    self.quasdsContainer.layer.borderWidth = 8.0;
    self.quasdsContainer.layer.cornerRadius = 5.0;
    
    if (![NSUserDefaults standardUserDefaults].quizHelp && !self.demoVersion) {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Tap the correct answer to reval parts of the photo and then guess what is below. Good luck!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Don't Show Again", nil] show];
    }
    
    NSTimeInterval addTime = [NSUserDefaults standardUserDefaults].seconds ? 20 : 0;
    
    self.timerView.timerLabel.text = [NSString stringWithFormat:@"%@", @(TIMER_QUIZ_START_COUNT+addTime)];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag != 999) {
        [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    }
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [NSUserDefaults standardUserDefaults].quizHelp = YES;
            break;
            
        default:
            break;
    }
}



-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y -= size.height;
        self.controllContainerView.frame = frame;
    }completion:Nil];
    
}

-(void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y += size.height;
        self.controllContainerView.frame = frame;
    }completion:nil];
    
}






-(void)imageDidLoad:(UIImage *)image
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.image = image;
        
        CGFloat rationX = image.size.width/image.size.height;
        CGFloat rationY = image.size.height/image.size.width;
        
        
        CGFloat width = self.imageView.frame.size.width ;
        CGFloat height = self.imageView.frame.size.height;
        
        if (rationY < rationX) {
            height = self.imageView.frame.size.width * rationY;
        } else{
            width = self.imageView.frame.size.height * rationX;
        }
        
        CGRect frame = CGRectMake((self.imageView.frame.size.width-width)/2, (self.imageView.frame.size.height-height)/2+8, width, height-16);

        
        [self setupBlackQuads:frame];

        self.imageView.image = image;
        if (!self.demoVersion) {
            [self.timerView startTimer:TIMER_QUIZ_START_COUNT];
        }

        [self nextQuestion];
    });
}

- (void)startWithImage:(UIImage*)img{

}

- (void) setupBlackQuads:(CGRect) frame
{
    for (int i = 0; i<4; i++) {
        for (int j = 0; j<4; j++) {
            UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x + (i*frame.size.width/4), frame.origin.y + (j*frame.size.height/4), frame.size.width/4, frame.size.height/4)];
            view.backgroundColor = [UIColor blackColor];//[UIColor colorWithRed:82.0/255.0 green:187.0/255.0 blue:252.0/255.0 alpha:1];
            view.image = [UIImage imageNamed:@"TAP REMOVE SQUARE"];
            view.layer.borderColor = [UIColor colorWithRed:0.302 green:0.651 blue:1 alpha:1.0].CGColor;
            view.layer.borderWidth = 0;
            view.userInteractionEnabled = YES;
            
//            UIButton *quad = [UIButton buttonWithType:UIButtonTypeCustom];
//            [quad addTarget:self action:@selector(removeQuad:) forControlEvents:UIControlEventTouchUpInside];
//            quad.frame = CGRectMake(1, 1, frame.size.width/4-2, frame.size.height/4-2);
//            quad.backgroundColor = [UIColor clearColor];
//            
//            [view addSubview:quad];
            [self.quasdsContainer addSubview:view];
        }
    }
}

- (void) removeQuad:(UIImageView*) sender
{
//    if (_rightAnswers <= 0) {
//        return;
//    }
//    _rightAnswers--;
    [UIView animateWithDuration:0.4 animations:^{
        sender.alpha = 0.0;
    } completion:^(BOOL finished) {
        [sender removeFromSuperview];
    }];
}





- (void) nextQuestion
{
    for (UIView *view in self.questionsContainerView.subviews) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect frame = view.frame;
            frame.origin.x -= frame.size.width;
            view.frame = frame;
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }
    
    if (self.questions.count == _questIndex) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, -30, 300, 150)];
        label.text = @"You're Fresh Out of Questions!";
        label.font = [UIFont systemFontOfSize:24];
        label.textAlignment = NSTextAlignmentCenter;
        label.alpha = 0.0;
        [self.questionsContainerView addSubview:label];
        [UIView animateWithDuration:0.4 animations:^{
            label.alpha = 1.0;
        }];
        return;
    }
    
    GWQuestionsView *questionView = [GWQuestionsView viewWithDelegate:self andQuestion:self.questions[_questIndex]];
    CGRect frame = questionView.frame;
    frame.origin.x += frame.size.width;
    questionView.frame = frame;
    [self.questionsContainerView addSubview:questionView];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = questionView.frame;
        frame.origin.x -= frame.size.width;
        questionView.frame = frame;
    }];
    
    _questIndex++;
}

-(void)questionViewDidPressedRightAnswer:(GWQuestionsView *)view
{
//    _rightAnswers++;
    if (self.quasdsContainer.subviews.count > 0) {
        double val = ((double)arc4random() / ARC4RANDOM_MAX);
        NSInteger randomIndex = self.quasdsContainer.subviews.count * val;
        UIImageView *image = self.quasdsContainer.subviews[randomIndex];
        [self removeQuad:image];
    }
    
    [[[GWAudioPlayer alloc] initWithSound:@"Harp"]playIfSoundisEnabled];
    [self showCorrectView];
    [self nextQuestion];
}

-(void)questionViewDidPressedWrongAnswer:(GWQuestionsView *)view
{
    [[[GWAudioPlayer alloc] initWithSound:@"Quack"]playIfSoundisEnabled];
    [self showIncorectView];
    [self nextQuestion];
}


- (void) showCorrectView
{
    UIView * view = [[UIView alloc] initWithFrame:self.view.frame];
    view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    view.alpha = 0.0;
    [self.view addSubview:view];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 140)];
    image.center = CGPointMake(image.center.x +220, image.center.y +300);
    image.image = [UIImage imageNamed:@"CORRECT"];
    image.userInteractionEnabled = NO;
    [view addSubview:image];
    
    [UIView animateWithDuration:0.15 animations:^{
        view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 delay:0.25 options:0 animations:^{
            view.alpha = 0.0;
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissPopup:)];
    
    [view addGestureRecognizer:tap];
    
}

- (void) showIncorectView
{
    UIView * view = [[UIView alloc] initWithFrame:self.view.frame];
    view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    view.alpha = 0.0;
    [self.view addSubview:view];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 140)];
    image.center = CGPointMake(image.center.x +220, image.center.y +300);
    image.image = [UIImage imageNamed:@"INCORRECTQUIZ"];
    image.userInteractionEnabled = NO;
    [view addSubview:image];
    
    [UIView animateWithDuration:0.15 animations:^{
        view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 delay:0.25 options:0 animations:^{
            view.alpha = 0.0;
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissPopup:)];
    
    [view addGestureRecognizer:tap];
    
}

- (void) dismissPopup:(UITapGestureRecognizer*) tap
{
    UIView *view = tap.view;
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}




@end
