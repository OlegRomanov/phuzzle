//
//  GWMainNavigationViewController.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWTitleBar;

@interface GWMainNavigationViewController : UINavigationController

@property (nonatomic, strong) GWTitleBar *bar;

@end
