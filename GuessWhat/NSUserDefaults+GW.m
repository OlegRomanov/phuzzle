//
//  NSUserDefaults+GW.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "NSUserDefaults+GW.h"

NSString* const kDeviceToken = @"kDeviceToken";
NSString* const kDevicePuzzleType = @"kDevicePuzzleType";

NSString* const kUserIDCurrent = @"kUserIDCurrent";

NSString* const kSessionToken = @"kSessionToken";
NSString* const kUsername = @"kUsername";
NSString* const kPassword = @"kPassword";

NSString* const kFacebookSession = @"kFacebookSession";
NSString* const kFacebookID = @"kFacebookID";

NSString* const kRecivedPuzzleID = @"kRecivedPuzzleID";

NSString* const kPauseDate = @"kPauseDate";

NSString* const kRememberSession = @"kRememberSession";

@implementation NSUserDefaults (GW)

- (void) logout
{
    NSString *devicePuzzleType = [self devicePuzzleType];
    BOOL review = [self review];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [self removePersistentDomainForName:appDomain];
    [self synchronize];
    
    [self setDevicePuzzleType:devicePuzzleType];
    [self setReview:review];
    
}

-(NSString *)devicePuzzleType
{
    return [self objectForKey:kDevicePuzzleType];
}

-(void)setDevicePuzzleType:(NSString *)devicePuzzleType
{
    [self setObject:devicePuzzleType forKey:kDevicePuzzleType];
    [self synchronize];
}


-(NSString *)deviceToken
{
    return [self objectForKey:kDeviceToken];
}

-(void)setDeviceToken:(NSString *)deviceToken
{
    [self setObject:deviceToken forKey:kDeviceToken];
    [self synchronize];
}

-(NSNumber *)currentUserID
{
    return [self objectForKey:kUserIDCurrent];
}

-(void)setCurrentUserID:(NSNumber *)currentUserID
{
    [self setObject:currentUserID forKey:kUserIDCurrent];
    [self synchronize];
}

-(NSString *)sessionToken
{
    return [self objectForKey:kSessionToken];
}

-(void)setSessionToken:(NSString *)sessionToken
{
    [self setObject:sessionToken forKey:kSessionToken];
    [self synchronize];
}

-(NSString *)username
{
    return [self objectForKey:kUsername];
}

-(void)setUsername:(NSString *)username
{
    [self setObject:username forKey:kUsername];
    [self synchronize];
}

-(NSString *)password
{
    return [self objectForKey:kPassword];
}

-(void)setPassword:(NSString *)password
{
    [self setObject:password forKey:kPassword];
    [self synchronize];
}

-(void)setFacebookSession:(BOOL)facebookSession
{
    [self setObject:@(facebookSession) forKey:kFacebookSession];
    [self synchronize];
}

-(BOOL)facebookSession
{
    return [[self objectForKey:kFacebookSession] boolValue];
}

-(void)setFacebookID:(NSNumber *)facebookID
{
    [self setObject:facebookID forKey:kFacebookID];
    [self synchronize];
}

-(NSNumber *)facebookID
{
    return [self objectForKey:kFacebookID];
}

-(void)setRecivedPuzzleID:(NSNumber *)recivedPuzzleID
{
    [self setObject:recivedPuzzleID forKey:kRecivedPuzzleID];
    [self synchronize];
}

-(NSNumber *)recivedPuzzleID
{
    return [self objectForKey:kRecivedPuzzleID];
}


-(NSDate *)startTimerTime
{
    return [self objectForKey:kPauseDate];
}

-(void)setStartTimerTime:(NSDate *)pauseDate
{
    [self setObject:pauseDate forKey:kPauseDate];
    [self synchronize];
}

-(void)setRememberSession:(BOOL)rememberSession
{
    [self setObject:@(rememberSession) forKey:kRememberSession];
    [self synchronize];
}

-(BOOL)rememberSession
{
    return [[self objectForKey:kRememberSession] boolValue];
}

-(void)setSoundStatus:(BOOL)soundStatus
{
    [self setObject:@(soundStatus) forKey:@"soundStatus"];
    [self synchronize];
}

-(BOOL)soundStatus
{
    return [[self objectForKey:@"soundStatus"] boolValue];
}

-(BOOL) quizHelp
{
    return [[self objectForKey:@"quizHelp"] boolValue];
}

- (void) setQuizHelp:(BOOL)quizHelp
{
    [self setObject:@(quizHelp) forKey:@"quizHelp"];
    [self synchronize];
}

-(BOOL)spyholeHelp
{
    return [[self objectForKey:@"spyholeHelp"] boolValue];
}

- (void)setSpyholeHelp:(BOOL)spyholeHelp
{
    [self setObject:@(spyholeHelp) forKey:@"spyholeHelp"];
    [self synchronize];
}

-(BOOL)review
{
    return [[self objectForKey:@"review"] boolValue];
}

- (void)setReview:(BOOL)review
{
    [self setObject:@(review) forKey:@"review"];
    [self synchronize];
}

-(BOOL)seconds
{
    return [[self objectForKey:@"seconds"] boolValue];
}

- (void)setSeconds:(BOOL)seconds
{
    [self setObject:@(seconds) forKey:@"seconds"];
    [self synchronize];
}

-(BOOL)shouldStartGame
{
    return [[self objectForKey:@"shouldStartGame"] boolValue];
}

- (void)setShouldStartGame:(BOOL)shouldStartGame
{
    [self setObject:@(shouldStartGame) forKey:@"shouldStartGame"];
    [self synchronize];
}

@end
