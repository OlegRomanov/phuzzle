//
//  GWDataLoader.m
//  Phuzzle
//
//  Created by Artem Antipov on 20.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWDataLoader.h"
#import "AFNetworking.h"

#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"
#import "GWQuestion+Methods.h"

#import "Reachability.h"

NSString *const kNotificationRecivePuzzle = @"kNotificationRecivePuzzle";

//#define SERVER_URL @"http://guesswhat"
#define SERVER_URL @"http://ec2-54-209-139-97.compute-1.amazonaws.com"

@interface GWDataLoader ()
@property(nonatomic, retain) NSMutableArray *gamesArray;
@end

@implementation GWDataLoader

+ (GWDataLoader *)sharedInstance {
    static dispatch_once_t once;
    static GWDataLoader *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        Reachability *reach = [Reachability reachabilityWithHostname:@"www.google.com"];

        // Set the blocks
        reach.reachableBlock = ^(Reachability *reach) {
            NSLog(@"REACHABLE!");
            _internetConnected = YES;
        };

        reach.unreachableBlock = ^(Reachability *reach) {
            NSLog(@"UNREACHABLE!");
            _internetConnected = NO;
        };

        [reach startNotifier];
    }
    return self;
}

- (void)dealloc {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(registerDeviceToken) object:nil];
}


- (void)loginWithUsername:(NSString *)username withPassword:(NSString *)pass andCompletinoBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    if (!username || !pass) {
        block([NSError errorWithDomain:@"" code:0 userInfo:@{}], nil);
        return;
    }
    NSString *URLString = [NSString stringWithFormat:@"%@/sign_in", SERVER_URL];
    NSDictionary *params = @{@"username" : username, @"password" : pass};
    NSDictionary *parameters = @{@"player" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {
            NSDictionary *profile = responseObject[@"profile"];

            NSString *token = profile[@"authentication_token"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [NSUserDefaults standardUserDefaults].currentUserID = userProfile.id;
            [NSUserDefaults standardUserDefaults].sessionToken = token;
            [NSUserDefaults standardUserDefaults].username = username;
            [NSUserDefaults standardUserDefaults].password = pass;

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];

            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

            if (block) {
                block(nil, userProfile);
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];

}

- (void)loginWithFacebook:(NSString *)username withID:(NSNumber *)fbID andCompletinoBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    NSString *URLString = [NSString stringWithFormat:@"%@/facebook_auth", SERVER_URL];
    NSDictionary *params = @{@"username" : username, @"facebook_id" : fbID};
    NSDictionary *parameters = @{@"player" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {
            NSDictionary *profile = responseObject[@"profile"];

            NSString *token = profile[@"authentication_token"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [NSUserDefaults standardUserDefaults].currentUserID = userProfile.id;
            [NSUserDefaults standardUserDefaults].sessionToken = token;
            [NSUserDefaults standardUserDefaults].username = username;
            [NSUserDefaults standardUserDefaults].facebookID = fbID;
            [NSUserDefaults standardUserDefaults].facebookSession = YES;

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];

            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

            if (block) {
                block(nil, userProfile);
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];

}

- (void)registerWithUsername:(NSString *)username withPassword:(NSString *)pass withEmail:(NSString *)email andCompletinoBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    NSString *URLString = [NSString stringWithFormat:@"%@/sign_up", SERVER_URL];
    NSDictionary *params = @{@"username" : username, @"password" : pass, @"email" : email};
    NSDictionary *parameters = @{@"player" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {
//            NSDictionary *profile = responseObject[@"profile"];
//            
//            NSString * token = profile[@"authentication_token"];
//            NSDictionary *player = profile[@"player"];
//            
//            GWProfile *userProfile = [GWProfile createWithDict:player];
//            
//            [NSUserDefaults standardUserDefaults].currentUserID = userProfile.id;
//            [NSUserDefaults standardUserDefaults].sessionToken = token;
//            [NSUserDefaults standardUserDefaults].username = username;
//            [NSUserDefaults standardUserDefaults].password = pass;
//            
//            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
//            
//            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

            if (block) {
                block(nil, nil);
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }


    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];

}

- (void)logout {
    [GWProfile MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"id >= 0"]];
    [GWPuzzle MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"id >= 0"]];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(registerDeviceToken) object:nil];
    [self unregisterDeviceToken];
    [[GWFacebookManager sharedInstance] logout];
    [[NSUserDefaults standardUserDefaults] logout];


}

+ (NSError *)getError:(NSDictionary *)responseObject {
    NSError *error = nil;
    NSString *status = responseObject[@"status"];
    if (![[status lowercaseString] isEqualToString:@"ok"]) {
        error = [NSError errorWithDomain:@"GWDataLoaderError" code:-1000 userInfo:@{@"messages" : responseObject[@"messages"]}];
    }
    return error;
}

+ (UIAlertView *)alertViewForError:(NSError *)error {
    NSDictionary *message = error.userInfo[@"messages"][0];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:message[@"text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return alert;
}


- (void)uploadPuzzle:(GWPuzzle *)puzzle withImage:(UIImage *)image withProgressBlock:(GWProgressBlock)progessBlock andCompletinoBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required to send puzzle."}]}];
        block(error, nil);
        return;
    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{@"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken, @"puzzle" : [puzzle toDictionary]};
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation]};
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *URLString = [NSString stringWithFormat:@"%@/send_puzzle", SERVER_URL];
    long long timestamp = [[NSDate date] timeIntervalSince1970] * 100000;
    NSString *fileName = [NSString stringWithFormat:@"image-%lld.png", timestamp];
    AFHTTPRequestOperation *op = [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"img_file" fileName:fileName mimeType:@""];
    }                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {
            [puzzle MR_deleteEntity];

            [[puzzle managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, nil);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }
    }                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];

    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        progessBlock(totalBytesWritten, totalBytesExpectedToWrite);
    }];
}

- (void)sendPuzzleRezult:(GWPuzzle *)puzzle andCompletinoBlock:(GWComletionBlock)block {
    if (puzzle.id.integerValue == 0) {
        return;
    }
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{@"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"puzzle" : @{
                    @"id" : puzzle.id,
                    @"status" : puzzle.correct.boolValue ? @"resolved" : @"lose",
                    @"player_to" : puzzle.opponent.id,
                    @"coins" : puzzle.coins,
                    @"solved_for_key" : puzzle.keySolved.boolValue ? @1 : @0
            }
    };
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation]};
    NSLog(@"params: %@", parameters);
    NSString *URLString = [NSString stringWithFormat:@"%@/send_result", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];
//            [puzzle MR_deleteEntity];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, nil);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];

}

- (void)sendInviteToEmail:(NSString *)email withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
#warning remove player.id
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"send_email",
            @"ref_email" : email
    };
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSArray *games = responseObject[@"games"];
            if (games.count > 0) {
                NSDictionary *game = games[0];
                NSDictionary *player = game[@"apponent"];
                GWProfile *userProfile = [GWProfile createWithDict:player];
                GWPuzzle *puzzle = [GWPuzzle createWithDict:game[@"puzzle"]];
                userProfile.puzzle = puzzle;
                [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
                if (block) {
                    block(nil, userProfile);
                }
            } else {
                if (block) {
                    block(nil, nil);
                }
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

- (void)sendInviteToFacebook:(NSNumber *)fbID withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
#warning remove player.id
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"reff_facebook",
            @"facebook_ids" : @[fbID]
    };
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {
            if (block) {
                block(nil, nil);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

- (void)checkFBFriendsIDs:(NSArray *)ids withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"get_allowed_fb_ids",
            @"facebook_ids" : ids
    };
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSArray *gamesInfo = responseObject[@"games"];

            if (gamesInfo.count > 0) {
                NSMutableArray *players = [NSMutableArray array];

                for (NSDictionary *info in gamesInfo) {
                    GWProfile *profile = [GWProfile createWithDict:info[@"apponent"]];
                    GWPuzzle *puzzle = [GWPuzzle createWithDict:info[@"puzzle"]];
                    profile.puzzle = puzzle;
                    [players addObject:profile];
                    NSNumber *wins = info[@"higest_consecutive_wins"];
                    profile.higestConsecutiveWins = wins;
                }

                [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

                if (block) {
                    block(nil, players);
                }
            } else {
                if (block) {
                    block(nil, nil);
                }
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

- (void)buyGameType:(NSString *)gameType withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"unlock_game",
            @"unlock_type" : gameType
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, nil);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

- (void)loadPuzzleWithID:(NSNumber *)puzzleID withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    NSString *URLString = [NSString stringWithFormat:@"%@/get_puzzle", SERVER_URL];
    NSDictionary *params = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
    };
    NSDictionary *parameters = @{@"profile" : [params JSONRepresentation], @"puzzle_id" : puzzleID};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *puzzleInfo = profile[@"puzzle"];

            GWPuzzle *puzzle = [GWPuzzle createWithDict:puzzleInfo];

            [[puzzle managedObjectContext] MR_saveOnlySelfAndWait];

            if (block) {
                block(nil, puzzle);
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)loadAllPuzzlesWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    NSString *URLString = [NSString stringWithFormat:@"%@/get_puzzles", SERVER_URL];
    NSDictionary *params = @{@"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken};
    NSDictionary *parameters = @{@"profile" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSArray *puzzlesInfo = profile[@"puzzles"];

            NSMutableArray *puzzles = [NSMutableArray arrayWithCapacity:puzzlesInfo.count];
            for (NSDictionary *dict in puzzlesInfo) {
                GWPuzzle *puzzle = [GWPuzzle createWithDict:dict];
                [puzzles addObject:puzzle];
            }

            [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

            if (block) {
                block(nil, puzzles);
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)loadAllGamesWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    NSString *URLString = [NSString stringWithFormat:@"%@/get_games", SERVER_URL];
    NSDictionary *params = @{@"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken};
    NSLog(@"params: %@", params);
    NSDictionary *parameters = @{@"profile" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {

            NSArray *gamesInfo = responseObject[@"games"];
            self.gamesArray = nil;
            self.gamesArray = [[NSMutableArray alloc] initWithArray:gamesInfo];
            if (gamesInfo.count > 0) {
                NSMutableArray *players = [NSMutableArray arrayWithCapacity:gamesInfo.count];

                for (NSDictionary *info in gamesInfo) {
                    GWProfile *profile = [GWProfile createWithDict:info[@"apponent"]];
                    GWPuzzle *puzzle = [GWPuzzle createWithDict:info[@"puzzle"]];
                    profile.puzzle = puzzle;
                    [players addObject:profile];
                    NSNumber *wins = info[@"higest_consecutive_wins"];
                    profile.higestConsecutiveWins = wins;
                    NSNumber* gameID = info[@"id"];
                    profile.gameIdentify = gameID;
                    NSLog(@"gameIdentify: %d", [profile.gameIdentify intValue]);
                }

                [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

                if (block) {
                    block(nil, players);
                }
            } else {
                if (block) {
                    block(nil, nil);
                }
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)registerDeviceToken {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"apns_token" : [NSUserDefaults standardUserDefaults].deviceToken};
    NSString *URLString = [NSString stringWithFormat:@"%@/set_apns_device", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {
        } else {
            [self performSelector:@selector(registerDeviceToken) withObject:nil afterDelay:10];
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self performSelector:@selector(registerDeviceToken) withObject:nil afterDelay:10];
    }];
}

- (void)unregisterDeviceToken {
    if (![NSUserDefaults standardUserDefaults].deviceToken) {
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"apns_token" : [NSUserDefaults standardUserDefaults].deviceToken};
    NSString *URLString = [NSString stringWithFormat:@"%@/reset_apns_device", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {
        } else {
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)adsClick {

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"ads_click",
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)buyKeysWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        if (block) {
            block(error, nil);
        }
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"buy_keys",
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, userProfile);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getKeyWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        if (block) {
            block(error, nil);
        }
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"add_keys",
            @"count_keys" : @1,
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, userProfile);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getKeys:(NSNumber *)count WithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        if (block) {
            block(error, nil);
        }
        return;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *profile = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"add_keys",
            @"count_keys" : count,
    };
    NSDictionary *parameters = @{@"profile" : [profile JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
    }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSDictionary *profile = responseObject[@"profile"];
            NSDictionary *player = profile[@"player"];

            GWProfile *userProfile = [GWProfile createWithDict:player];

            [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
            if (block) {
                block(nil, userProfile);
            }
        } else {
            if (block) {
                block(error, nil);
            }
        }

    }     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


- (void)loadQuestoinsWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }
    NSString *URLString = [NSString stringWithFormat:@"%@/get_questions", SERVER_URL];
    NSDictionary *params = @{@"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken};
    NSLog(@"params: %@", params);
    NSDictionary *parameters = @{@"profile" : [params JSONRepresentation]};
    NSError *error = nil;
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:parameters error:&error];

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];

        if (!error) {

            NSArray *questions = responseObject[@"questions"];

            if (questions.count > 0) {
                NSMutableArray *quests = [NSMutableArray arrayWithCapacity:questions.count];

                for (NSDictionary *info in questions) {
                    GWQuestion *question = [GWQuestion createEntryForID:info[@"id"]];
                    question.question = info[@"text"];
                    question.answers = info[@"answers"];
                    question.correctAnswer = info[@"correct_answer"];

                    [quests addObject:question];
                }

                [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];

                if (block) {
                    block(nil, quests);
                }
            } else {
                if (block) {
                    block(nil, nil);
                }
            }
        } else {

            if (block) {
                block(error, nil);
            }
        }

    }                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        if (block) {
            block(error, nil);
        }
    }];

    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)deleteGame:(NSNumber *)gameID withCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    if (gameID) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *jsonDict = @{
                @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
                @"player" : @{@"id" : [GWProfile currentUser].id}
        };
        NSDictionary *action = @{
                @"type" : @"delete_game_state",
                @"game_state_id" : gameID
        };
        NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation], @"action" : [action JSONRepresentation]};
        NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
        [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
        }     success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Success: %@", responseObject);

            NSError *error = [GWDataLoader getError:responseObject];
            if (!error) {

                NSString *status = responseObject[@"status"];
                bool success = NO;
                if ([status isEqualToString:@"ok"] || [status isEqualToString:@"OK"]) {
                    success = YES;
                }
                if (block) {
                    block(nil, [NSNumber numberWithBool:success]);
                }
            } else {
                if (block) {
                    block(error, nil);
                }
            }
    }
    failure:
    ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

}

- (void)loadRandomPlayerWithCompletionBlock:(GWComletionBlock)block {
    if (!self.internetConnected) {
        NSError *error = [NSError errorWithDomain:@"internet.connected.failed" code:1000 userInfo:@{NSLocalizedDescriptionKey : @"An internet connection is required to send puzzles.", @"messages" : @[@{@"text" : @"An internet connection is required."}]}];
        block(error, nil);
        return;
    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *jsonDict = @{
            @"authentication_token" : [NSUserDefaults standardUserDefaults].sessionToken,
            @"player" : @{@"id" : [GWProfile currentUser].id}
    };
    NSDictionary *action = @{
            @"type" : @"get_random_player",
    };
    NSDictionary *parameters = @{@"profile" : [jsonDict JSONRepresentation], @"action" : [action JSONRepresentation]};
    NSString *URLString = [NSString stringWithFormat:@"%@/action", SERVER_URL];
    [manager GET:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);

        NSError *error = [GWDataLoader getError:responseObject];
        if (!error) {

            NSString *status = responseObject[@"status"];
            bool success = NO;
            if ([status isEqualToString:@"ok"] || [status isEqualToString:@"OK"]) {
                NSArray *games = responseObject[@"games"];
                if (games.count > 0) {
                    NSDictionary *game = games[0];
                    NSDictionary *player = game[@"apponent"];
                    GWProfile *userProfile = [GWProfile createWithDict:player];
                    GWPuzzle *puzzle = [GWPuzzle createWithDict:game[@"puzzle"]];
                    userProfile.puzzle = puzzle;
                    [[userProfile managedObjectContext] MR_saveOnlySelfAndWait];
                    if (block) {
                        block(nil, userProfile);
                    }
                } else {
                    if (block) {
                        block(nil, nil);
                    }
                }
            } else {
                if (block) {
                    block(error, nil);
                }
            }

        }

    }    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block(error, nil);
        }
    }];
}

@end
