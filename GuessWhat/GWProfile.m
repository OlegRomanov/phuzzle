//
//  GWProfile.m
//  Phuzzle
//
//  Created by Artem Antipov on 26.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWProfile.h"
#import "GWPuzzle.h"


@implementation GWProfile

@dynamic adClicks;
@dynamic coins;
@dynamic email;
@dynamic facebookID;
@dynamic higestConsecutiveWins;
@dynamic id;
@dynamic keys;
@dynamic quizUnlocked;
@dynamic referrals;
@dynamic spyholeUnlocked;
@dynamic username;
@dynamic tilesUnlocked;
@dynamic puzzle;
@dynamic gameIdentify;

@end
