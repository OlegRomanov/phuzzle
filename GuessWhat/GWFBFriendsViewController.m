//
//  GWFBFriendsViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 23.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWFBFriendsViewController.h"
#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"

#import "GWTilesGameViewController.h"
#import "GWSpyholeGameViewController.h"
#import "GWQuizGameViewController.h"

@interface GWFBFriendsViewController ()
<UITableViewDataSource, UITableViewDelegate, GWCameraViewControllerDelegate, UITextFieldDelegate>
{
    BOOL _play;
    BOOL _isLoad;
    BOOL _sharing;
    UITapGestureRecognizer *_tap;
    
    BOOL _logout;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UIButton *inviteButton;
@property (strong, nonatomic) IBOutlet GWTextField *searchField;

@property(nonatomic , strong) NSArray* fbFriends;
@property(nonatomic , strong) NSMutableArray* friendsIn;
@property(nonatomic , strong) NSMutableArray* friendsOut;
@property(nonatomic , strong) NSArray* inIDS;
@property(nonatomic , strong) NSArray* filtered;

@end

@implementation GWFBFriendsViewController
-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.friendsIn = [NSMutableArray array];
    self.friendsOut = [NSMutableArray array];
    
	
    
    
    
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_isLoad) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    } else {
        _isLoad = YES;
    }
    
    _logout = NO;
    
    // Do any additional setup after loading the view.
    if ([GWFacebookManager sharedInstance].isSessionOpen || [GWFacebookManager sharedInstance].isSessionExtended) {
        [self loadFriends];
    } else {
        [[GWFacebookManager sharedInstance] loginWithCompletionBlock:^(id error, BOOL success) {
            
        }];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadFriends)
                                                 name:kFBNotificationLoggedIn
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginCanceled)
                                                 name:kFBNotificationLoggedCancel
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginCanceled)
                                                 name:kFBNotificationLoggedOut
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedIn
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedCancel
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedOut
                                                  object:nil];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    _tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:_tap];
}

-(void)keyboardWillHide:(NSNotification*)notification {
    [self.view removeGestureRecognizer:_tap];
}

- (void) loginCanceled
{
    if (_logout || _sharing) {
        return;
    }
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    _logout = YES;
//    [self backPressed:self];
    
    [self performSelector:@selector(backPressed:) withObject:nil afterDelay:1.0];
//    [[[UIAlertView alloc] initWithTitle:nil message:@"Something went wrong message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) loadFriends
{
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    if (_logout || _sharing) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GWFacebookManager sharedInstance] fetchFriendsWithCompletionBlock:^(id error, id result) {
        if (!error) {
            _play = YES;
            self.fbFriends = result[@"data"];
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                           ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            self.fbFriends = [self.fbFriends sortedArrayUsingDescriptors:sortDescriptors];
            
            NSMutableArray *ids = [NSMutableArray array];
            for (NSDictionary *friend in self.fbFriends) {
                [ids addObject:friend[@"id"]];
            }
            [[GWDataLoader sharedInstance] checkFBFriendsIDs:ids withCompletionBlock:^(id error, id result) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (error) {
                    self.inIDS = @[];
                } else {
                    self.inIDS = result;
                    [self filterFriends];
                    self.filtered = self.friendsIn;
                }
                [self.tableView reloadData];
            }];
        } else {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) filterFriends
{
    BOOL inner = NO;
    for (NSDictionary *friend in self.fbFriends) {
        for (GWProfile *profile in self.inIDS) {
            if ([friend[@"id"] longLongValue] == profile.facebookID.longLongValue) {
                inner = YES;
                break;
            }
        }
        if (!inner) {
            [self.friendsOut addObject:friend];
        } else {
            [self.friendsIn addObject:friend];
        }
        inner = NO;
    }
    
//    self.friendsIn = [[self separateByGroup:self.friendsIn] mutableCopy];
//    self.friendsOut = [[self separateByGroup:self.friendsOut] mutableCopy];
    
}

//- (NSArray*) separateByGroup:(NSArray*) friends
//{
//    NSMutableArray *sections = [NSMutableArray array];
//    
//    NSMutableDictionary *sectionsDict = [NSMutableDictionary dictionary];
//    for (NSDictionary *dict in friends) {
//        NSString *key = [[dict[@"name"] substringToIndex:1] uppercaseString];
//        NSMutableArray *sec = sectionsDict[key];
//        if (sec) {
//            [sec addObject:dict];
//        } else {
//            sec = [NSMutableArray array];
//            [sec addObject:dict];
//            [sectionsDict setObject:sec forKey:key];
//        }
//    }
//    
//    NSArray *keys = [sectionsDict allKeys];
//    
//    for (NSString *key in keys) {
//        NSArray *section = sectionsDict[key];
//        [sections addObject:@{@"key":key, @"items":section}];
//    }
//    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"key" ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//    return [[NSArray arrayWithArray:sections] sortedArrayUsingDescriptors:sortDescriptors];
//}

- (GWProfile*) findProfileWithFBID:(NSNumber*) fbID
{
    for (GWProfile *profile in self.inIDS) {
        if ([fbID longLongValue] == profile.facebookID.longLongValue) {
            return profile;
        }
    }
    return nil;
}

- (IBAction)sharePressed:(id)sender
{
    UIView * view = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
    view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    view.alpha = 0.0;
    [self.navigationController.view addSubview:view];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 280, 280)];
    image.image = [UIImage imageNamed:@"SHAREBORDER"];
    image.userInteractionEnabled = NO;
    [view addSubview:image];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(55, 65, 210, 190)];
    textView.editable = YES;
    textView.tag = 1010;
    textView.backgroundColor = [UIColor clearColor];
    [view addSubview:textView];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(150, 230, 100, 55);
    [cancelButton setImage:[UIImage imageNamed:@"CANCEL"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelShare:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancelButton];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(80, 230, 55, 55);
    [shareButton setImage:[UIImage imageNamed:@"OK"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareShare:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:shareButton];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 1.0;
        [textView becomeFirstResponder];
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissPopup:)];
    
    [view addGestureRecognizer:tap];
}

- (void) shareShare:(UIButton*) sender
{
    if (_sharing) {
        return;
    }
    _sharing = YES;
    UIView *view = sender.superview;
    UITextView *textView = (UITextView*)[view viewWithTag:1010];
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[GWFacebookManager sharedInstance] postToWall:textView.text withCompletionBlock:^(NSError* error, id result) {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        if (!error) {
            [UIView animateWithDuration:0.35 animations:^{
                view.alpha = 0.0;
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
            }];
        }
        
        if (!_sharing) {
            return ;
        }
        
        if (!error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Shared!" message:@"Your post was successfuly shared on your Facebook wall!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        } else {
            NSString * alertText = error.localizedDescription;
            if (error.code != 10000) {
                alertText = [FBErrorUtility userMessageForError:error];
                if (!alertText) {
                    alertText = [GWFacebookManager errorMessage:error];
                }
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:alertText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        _sharing = NO;
    }];
}
- (void) cancelShare:(UIButton*) sender
{
    UIView *view = sender.superview;
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}
- (void) dismissPopup:(UITapGestureRecognizer*) tap
{
    UIView *view = tap.view;
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

- (IBAction)filterChanged:(id)sender
{
    self.playButton.selected = NO;
    self.inviteButton.selected = NO;
    if (sender == self.playButton) {
        self.playButton.selected = YES;
        self.inviteButton.selected = NO;
        _play = YES;
        self.filtered = self.friendsIn;
    } else {
        self.playButton.selected = NO;
        self.inviteButton.selected = YES;
        _play = NO;
        self.filtered = self.friendsOut;
    }
    [self.tableView reloadData];
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TAbleView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self dismissKeyboard];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return self.filtered.count;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filtered/*[section][@"items"]*/ count];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0;
//}
//
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return self.filtered[section][@"key"];
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GWFBFriendCell"];
    
    NSDictionary *friend = self.filtered/*[indexPath.section][@"items"]*/[indexPath.row];
    NSString *name = friend[@"name"];
    NSNumber *idNumber = friend[@"id"];
    
    UILabel *nameLabel = (UILabel*)[cell viewWithTag:1001];
    nameLabel.text = name;
    nameLabel.font = [UIFont defaultFontWithSize:nameLabel.font.pointSize];
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:1000];
    imageView.layer.borderWidth = 1.0f;
    imageView.layer.masksToBounds = NO;
    imageView.layer.cornerRadius = 30;
    imageView.clipsToBounds = YES;
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=square",idNumber]]];

    UIImageView *playButton = (UIImageView*)[cell viewWithTag:1002];
    [[playButton.superview viewWithTag:111] removeFromSuperview];
    if (_play) {
        GWProfile *profile = [self findProfileWithFBID:friend[@"id"]];
        GWPuzzle *puzzle = profile.puzzle;
        if (puzzle) {
            if (puzzle.my.boolValue) {
                UILabel *label = [[UILabel alloc] initWithFrame:playButton.frame];
                label.text = @"WAITING...";
                label.font = [UIFont defaultFontWithSize:14];
                [label sizeToFit];
                label.center = playButton.center;
                label.textColor = [UIColor whiteColor];
                label.tag = 111;
                label.textAlignment = NSTextAlignmentCenter;
                [playButton.superview addSubview: label];
                playButton.image = [UIImage imageNamed:@""];
            } else {
                playButton.image = [UIImage imageNamed:@"SOLVE BUTTON"];
            }
        } else {
            playButton.image = [UIImage imageNamed:@"FBKPLAYBUTTON"];
        }
    } else {
        [playButton setImage:[UIImage imageNamed:@"FBKINVITEBUTTON"]];
    }
    
    return cell;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
//}
//
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
//{
//    //sectionForSectionIndexTitleAtIndex: is a bit buggy, but is still useable
//    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *profileInfo = self.filtered/*[indexPath.section][@"items"]*/[indexPath.row];
    if (_play) {
        GWProfile *profile = [self findProfileWithFBID:profileInfo[@"id"]];
//        [self createPuzzleWithProfile:profile];
        if (profile.puzzle) {
            GWPuzzle *puzzle = profile.puzzle;
            if (puzzle.my.boolValue) {
                //            gameLabel.text = @"Waiting";
            } else {
                //            gameLabel.text = @"Resolve";
                if ([NSUserDefaults standardUserDefaults].recivedPuzzleID.longLongValue == puzzle.id.longLongValue) {
                    [NSUserDefaults standardUserDefaults].recivedPuzzleID = @0;
                }
                if ([puzzle.type isEqualToString:kPuzzleTypeTiles]) {
                    GWTilesGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWTilesGameViewController"];
                    vc.puzzle = puzzle;
                    [self.navigationController pushViewController:vc animated:YES];
                } else if ([puzzle.type isEqualToString:kPuzzleTypeSpyhole]) {
                    GWSpyholeGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWSpyholeGameViewController"];
                    vc.puzzle = puzzle;
                    [self.navigationController pushViewController:vc animated:YES];
                } else if ([puzzle.type isEqualToString:kPuzzleTypeQuiz]) {
                    GWQuizGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWQuizGameViewController"];
                    vc.puzzle = puzzle;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
        } else {
            //        gameLabel.text = @"Play";
            [self createPuzzleWithProfile:profile];
        }
    } else {
        [self inviteFBProfile:profileInfo];
    }
}

#pragma mark - Search

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([result length] == 0) {
        self.filtered = _play ? self.friendsIn : self.friendsOut;
    } else {
        NSMutableArray * filtered = [NSMutableArray array];
        for (NSDictionary*friend in _play ? self.friendsIn : self.friendsOut) {
            if ([[friend[@"name"] uppercaseString] rangeOfString:[result uppercaseString]].location != NSNotFound) {
                [filtered addObject:friend];
            }
        }
        self.filtered = filtered;
    }
    [self.tableView reloadData];
    return YES;
}

- (void) inviteFBProfile:(NSDictionary*)profile
{
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys: profile[@"id"], @"to", nil];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:[NSString stringWithFormat:@"Hey, you need to check this game out, it's so addictive. You can turn any photo to a puzzle, I've got some great ones I reckon you'll struggle on. Get the app so we can play!"]
                                                    title:@"This is a title"
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              NSLog(@"User canceled request.");
                                                          } else {
                                                              [[[GWAudioPlayer alloc] initWithSound:@"InviteSent"]playIfSoundisEnabled];
                                                              
                                                              [[[GWAudioPlayer alloc] initWithSound:@"InviteSent"]playIfSoundisEnabled];
                                                              //show invite sent
                                                              UIView * view = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
                                                              view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
                                                              view.alpha = 0.0;
                                                              [self.navigationController.view addSubview:view];
                                                              
                                                              UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 100)];
                                                              image.center = view.center;
                                                              image.image = [UIImage imageNamed:@"INVITESENT2"];
                                                              image.userInteractionEnabled = NO;
                                                              [view addSubview:image];
                                                              
                                                              [UIView animateWithDuration:0.15 animations:^{
                                                                  view.alpha = 1.0;
                                                              } completion:^(BOOL finished) {
                                                                  [UIView animateWithDuration:0.25 delay:1 options:0 animations:^{
                                                                      view.alpha = 0.0;
                                                                  } completion:^(BOOL finished) {
                                                                      [view removeFromSuperview];
                                                                  }];
                                                              }];
                                                              
                                                              UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                                                                             initWithTarget:self
                                                                                             action:@selector(dismissPopup:)];
                                                              
                                                              [view addGestureRecognizer:tap];
                                                              
                                                              NSLog(@"Request Sent.");
                                                              NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                                                              [f setNumberStyle:NSNumberFormatterDecimalStyle];
                                                              NSNumber * ID = [f numberFromString:profile[@"id"]];
                                                              [[GWDataLoader sharedInstance] sendInviteToFacebook:ID withCompletionBlock:^(id error, id result) {
                                                              }];
                                                          }
                                                      }}
                                              friendCache:nil];
}

- (void)createPuzzleWithProfile:(GWProfile*) profile {
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"NavControllerGWCameraViewController"];
    GWCameraViewController *vc = (GWCameraViewController*)nc.topViewController;
    vc.delegate = self;
    vc.profile = profile;
    
    [self presentViewController:nc animated:YES completion:nil];
}

-(void)didCapturedImage:(UIImage *)image withName:(NSString *)text toProfile:(GWProfile *)profile
{
    GWPuzzleTypeSelectViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleTypeSelectViewController"];
    vc.image = image;
    vc.whatIs = text;
    vc.profile = profile;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)captureImageDidDissmis
{
}

-(void)captureImageWillDissmis
{
}

@end
