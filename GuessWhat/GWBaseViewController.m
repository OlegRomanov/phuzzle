//
//  GWBaseViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"
#import "GWMainNavigationViewController.h"
#import "GWTitleBar.h"

#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"

#import "GWTilesGameViewController.h"
#import "GWSpyholeGameViewController.h"
#import "GWQuizGameViewController.h"


@interface GWBaseViewController ()
{
    BOOL adNeedPlay;
}


@end

@implementation GWBaseViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = YES;
    self.bgImageName = @"BACKGROUND5";
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setHidesBackButton:YES];
    [self setDefaultFont:self.view];
}

- (void) showADS
{
    RevMobFullscreen *ad = [[RevMobAds session] fullscreen]; // you must retain this object
    [ad loadWithSuccessHandler:^(RevMobFullscreen *fs) {
        [fs showAd];
        NSLog(@"Ad loaded");
    } andLoadFailHandler:^(RevMobFullscreen *fs, NSError *error) {
        NSLog(@"Ad error: %@",error);
    } onClickHandler:^{
        NSLog(@"Ad clicked");
        [[GWDataLoader sharedInstance] adsClick];
    } onCloseHandler:^{
        NSLog(@"Ad closed");
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self setDefaultFont: self.view];
    
}

- (void) setDefaultFont:(UIView*) superview
{
    for (UIView *view  in superview.subviews) {
        
        if ([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*) view;
            label.font = [UIFont defaultFontWithSize:label.font.pointSize];
        } else if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*) view;
            button.titleLabel.font = [UIFont defaultFontWithSize:button.titleLabel.font.pointSize];
        } else if ([view isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*) view;
            textField.font = [UIFont defaultFontWithSize:textField.font.pointSize];
        } else if ([view isKindOfClass:[UITextView class]]) {
            UITextView *textField = (UITextView*) view;
            textField.font = [UIFont defaultFontWithSize:textField.font.pointSize];
        } else {
            [self setDefaultFont:view];
        }
    }
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:self.tittleBarHidden animated:animated];
    
    if (!self.tittleBarHidden) {
        GWMainNavigationViewController *ng = (GWMainNavigationViewController*)self.navigationController;
        ng.bar.bgImage = [UIImage imageNamed:self.bgImageName];
    }
    
    
    if (!self.noADS) {
        double val = ((double)arc4random() / ARC4RANDOM_MAX);
        if (val <= 0.15f) {
            [self showADS];
        }
    }
    
    if (!self.notRecivePushes) {
        [self checkPuzzle];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.notRecivePushes) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkPuzzle) name:kNotificationRecivePuzzle object:nil];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (!self.notRecivePushes) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationRecivePuzzle object:nil];
    }
}

- (void) checkPuzzle
{
    if ([NSUserDefaults standardUserDefaults].recivedPuzzleID.longLongValue > 0) {
        [[GWDataLoader sharedInstance] loadPuzzleWithID:[NSUserDefaults standardUserDefaults].recivedPuzzleID withCompletionBlock:^(id error, id result) {
            if (!error) {
                if ([NSUserDefaults standardUserDefaults].shouldStartGame) {
                    [NSUserDefaults standardUserDefaults].shouldStartGame = NO;
                    self.recivedPuzzle = result;
                    [NSUserDefaults standardUserDefaults].recivedPuzzleID = @0;
                    [AdColony playVideoAdForZone:@"vzfc98660c1bcf48fa93" withDelegate:self];
                } else {
                    [[[GWAudioPlayer alloc] initWithSound:@"EarnKey"]playIfSoundisEnabled];
                    self.recivedPuzzle = result;
                    NSString * title = [NSString stringWithFormat:@" %@ wants to Phuzzle you, do you accept the challenge?", self.recivedPuzzle.opponent.username];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:@"Do you want play now?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                    alert.tag = 10999;
                    [alert show];
                    [NSUserDefaults standardUserDefaults].recivedPuzzleID = @0;
                }
            }
        }];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag != 10999) {
        return;
    }
    if (buttonIndex == 1) {
        NSLog(@"Start Game");
        //TODO COLONY replace with YOUR ZONE ID
        [AdColony playVideoAdForZone:@"vzfc98660c1bcf48fa93" withDelegate:self];
        
    } else {
        NSLog(@"Skip game now");
    }
}

#pragma mark - AdColonyAdDelegate

// Is called when AdColony has taken control of the device screen and is about to begin showing an ad
// Apps should implement app-specific code such as pausing a game and turning off app music
- ( void ) onAdColonyAdStartedInZone:( NSString * )zoneID {
    adNeedPlay = YES;
}

// Is called when AdColony has finished trying to show an ad, either successfully or unsuccessfully
// If shown == YES, an ad was displayed and apps should implement app-specific code such as unpausing a game and restarting app music
- ( void ) onAdColonyAdAttemptFinished:(BOOL)shown inZone:( NSString * )zoneID {
    adNeedPlay = NO;

	if (shown) {
        [self playPuzzle];
	} else {
		NSLog(@"AdColony did not play an ad for zone %@", zoneID);
        [self playPuzzle];
	}
}

- (void)playPuzzle:(GWPuzzle *)repeatPuzzle{
    self.recivedPuzzle = nil;
    self.recivedPuzzle = repeatPuzzle;
    [self playPuzzle];
}

- (void) playPuzzle
{
    if ([self.recivedPuzzle.type isEqualToString:kPuzzleTypeTiles]) {
        GWTilesGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWTilesGameViewController"];
        vc.puzzle = self.recivedPuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([self.recivedPuzzle.type isEqualToString:kPuzzleTypeSpyhole]) {
        GWSpyholeGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWSpyholeGameViewController"];
        vc.puzzle = self.recivedPuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([self.recivedPuzzle.type isEqualToString:kPuzzleTypeQuiz]) {
        GWQuizGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWQuizGameViewController"];
        vc.puzzle = self.recivedPuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        self.recivedPuzzle = nil;
    }
}

-(BOOL)shouldAutorotate
{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
