//
//  GWPuzzleSendViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPuzzleSendViewController.h"
#import "GWPuzzleSentViewController.h"
#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

@interface GWPuzzleSendViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation GWPuzzleSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.imageView.image = self.image;
    self.imageView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0.302 alpha:1.0].CGColor;
    self.imageView.layer.borderWidth = 2.0;
    self.imageView.layer.cornerRadius = 15.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendPressed:(id)sender {
    
    GWPuzzle *puzzle = [GWPuzzle MR_createEntity];
    puzzle.type = self.type;
    puzzle.answer = self.whatIs;
    puzzle.opponent = self.profile;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Sending...";
    [[GWDataLoader sharedInstance] uploadPuzzle:puzzle withImage:self.image withProgressBlock:^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        hud.progress = totalBytesWritten/totalBytesExpectedToWrite;
        
    } andCompletinoBlock:^(id error, id result) {
        [hud hide:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [puzzle MR_deleteEntity];
            GWPuzzleSentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleSentViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }];
    
}
@end
