//
//  NSArray+StringTOFROM.h
//  HeartLeaf
//
//  Created by Artem Antipov on 04.11.13.
//  Copyright (c) 2013 Nicecode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (StringTOFROM)

+ (NSArray*) arrayStringsWithString:(NSString*)string;
+ (NSArray*) arrayNumbersWithString:(NSString*)string;
//+ (NSArray*) arrayWithString:(NSString*)string;
- (NSString*) toString;

@end
