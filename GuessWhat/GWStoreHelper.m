//
//  GWStoreHelper.m
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWStoreHelper.h"

@implementation GWStoreHelper

+ (GWStoreHelper *)sharedInstance
{
    static dispatch_once_t once;
    static GWStoreHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.Whatthephuzzle.keys3",
                                      @"com.Whatthephuzzle.seconds30forever",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}


- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    [super completeTransaction:transaction];
    if ([@"com.Whatthephuzzle.keys3" isEqualToString:transaction.payment.productIdentifier]) {
        [self requestKeysFromServer:transaction];
    } else if ([@"com.Whatthephuzzle.seconds30forever" isEqualToString:transaction.payment.productIdentifier]) {
        [NSUserDefaults standardUserDefaults].seconds = YES;
        [[[UIAlertView alloc] initWithTitle:nil message:@"Congratulations! Your Phuzzle clock has been extended by 20 seconds!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        [self finishTransaction:transaction];
    }
}

- (void) requestKeysFromServer:(SKPaymentTransaction*) transaction
{

    [[GWDataLoader sharedInstance] buyKeysWithCompletionBlock:^(id error, id result) {
        if (error) {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Server Error. Your purchase will be completed later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            [self finishTransaction:transaction];
        }
    }];
}


@end
