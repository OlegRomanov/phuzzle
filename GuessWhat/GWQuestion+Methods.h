//
//  GWQuestion+Methods.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWQuestion.h"

@interface GWQuestion (Methods)

+ (GWQuestion*) createEntryForID:(NSNumber*) ID;

@end
