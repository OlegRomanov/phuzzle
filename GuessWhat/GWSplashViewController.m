//
//  GWViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWSplashViewController.h"
#import "GWMainViewController.h"

#import "GWProfile+Methods.h"

@interface GWSplashViewController ()

@end

@implementation GWSplashViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.noADS = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
//    if (![NSUserDefaults standardUserDefaults].devicePuzzleType) {
//        [self performSegueWithIdentifier:@"splashToPickPuzzle" sender:self];
//        return;
//    }
    [self checkAutoLogin];
}

- (void) checkAutoLogin
{
    if ([NSUserDefaults standardUserDefaults].rememberSession) {
        if ([NSUserDefaults standardUserDefaults].sessionToken) {
            if ([NSUserDefaults standardUserDefaults].facebookSession) {
                if (![GWFacebookManager sharedInstance].isSessionOpen) {
                    [[GWFacebookManager sharedInstance] loginWithCompletionBlock:^(id error, BOOL success) {
                        if (!error) {
                        } else {
                            [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
                            [[GWDataLoader sharedInstance] logout];
                        }
                    }];
                }
                [[GWDataLoader sharedInstance] loginWithFacebook:[NSUserDefaults standardUserDefaults].username withID:[NSUserDefaults standardUserDefaults].facebookID andCompletinoBlock:^(id error, id result) {
                    if (error) {
                        [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
                    } else {
                        [self performSelector:@selector(goToMain) withObject:nil afterDelay:0.5f];
                    }
                }];
            } else {
                [[GWDataLoader sharedInstance] loginWithUsername:[NSUserDefaults standardUserDefaults].username withPassword:[NSUserDefaults standardUserDefaults].password andCompletinoBlock:^(id error, id result) {
                    if (error) {
                        [GWDataLoader alertViewForError:error];
                        [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
                    } else {
                        [self performSelector:@selector(goToMain) withObject:nil afterDelay:0.5f];
                    }
                }];
            }
        } else {
            [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
        }
    } else {
        [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadFBProfile)
                                                 name:kFBNotificationLoggedIn
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedIn
                                                  object:nil];
}

-( void) loadFBProfile
{
    [[GWFacebookManager sharedInstance] fetchSelfWithCompletionBlock:^(id error, id result) {
        if (!error) {
            if (result) {
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                NSNumber * ID = [f numberFromString:result[@"id"]];
                [self fbLogin:result[kFBUsernameKey] andID:ID];
            }
            
        }
    }];
}

- (void) fbLogin:(NSString*)username andID:(NSNumber*)fbid
{
    [[GWDataLoader sharedInstance] loginWithFacebook:username withID:fbid andCompletinoBlock:^(id error, id result) {
        if (error) {
            [GWDataLoader alertViewForError:error];
            [self performSelector:@selector(goToLogin) withObject:nil afterDelay:0.5f];
        } else {
            GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
}

- (void) goToLogin
{
    [self performSegueWithIdentifier:@"goToLogin" sender:self];
}

- (void) goToMain
{
    GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
