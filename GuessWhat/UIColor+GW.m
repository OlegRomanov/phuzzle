//
//  UIColor+GW.m
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "UIColor+GW.h"

@implementation UIColor (GW)

+ (UIColor*) defaultTextColor
{
    return [UIColor colorWithRed:182.0/255.0 green:113.0/255.0 blue:252.0/255.0 alpha:1.0];
}

@end
