//
//  GWTextField.m
//  Phuzzle
//
//  Created by Artem Antipov on 22.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWTextField.h"

@implementation GWTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + self.horizontalPadding, bounds.origin.y + self.verticalPadding, bounds.size.width - self.horizontalPadding*2, bounds.size.height - self.verticalPadding*2);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

-(float)horizontalPadding
{
    return _horizontalPadding == 0 ? 20 : _horizontalPadding;
}                                                                                                                                                

@end
