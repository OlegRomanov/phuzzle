//
//  GWFacebookManager.m
//  Phuzzle
//
//  Created by Artem Antipov on 23.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWFacebookManager.h"
NSString* const kFBUsernameKey = @"name";
NSString* const kFBNotificationLoggedIn = @"kFBNotificationLoggedIn";
NSString* const kFBNotificationLoggedCancel = @"kFBNotificationLoggedCancel";
NSString* const kFBNotificationLoggedOut = @"kFBNotificationLoggedOut";

NSString* const kFBNotificationSharedToWall = @"kFBNotificationSharedToWall";
NSString* const kFBNotificationPostedToWall = @"kFBNotificationPostedToWall";

NSString* const kFBNotificationSharedToWallFailed = @"kFBNotificationSharedToWallFailed";
NSString* const kFBNotificationPostedToWallFailed = @"kFBNotificationPostedToWallFailed";

@implementation GWFacebookManager

+ (GWFacebookManager*)sharedInstance
{
    static dispatch_once_t once;
    static GWFacebookManager* sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void) lounch
{
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // This method will be called EACH time the session state changes,
                                          // also for intermediate states and NOT just when the session open
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
    }
    
}

- (void) handleDidBecomeActive
{
    [FBAppCall handleDidBecomeActive];
}

-(BOOL) openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
{
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    [FBSession.activeSession setStateChangeHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
         // Retrieve the app delegate
         [self sessionStateChanged:session state:state error:error];
     }];
    
    BOOL hadled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    return hadled;
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        if (self.postToWallBlock || self.shareToWallBlock) {
            if ([self checkPermissions]) {
                if (self.postToWallBlock) {
                    [self postToWall:self.postToWallText withCompletionBlock:self.postToWallBlock];
                    self.postToWallBlock = nil;
                    self.postToWallText = nil;
                }
                if (self.shareToWallBlock) {
                    [self sharedToWall:self.shareToWallText withImage:self.shareToWallImage withCompletionBlock:self.shareToWallBlock];
                    self.shareToWallBlock = nil;
                    self.shareToWallText = nil;
                    self.shareToWallImage = nil;
                }
            } else {
                if (self.postToWallBlock) {
                    self.postToWallBlock([NSError errorWithDomain:@"FBError" code:100000 userInfo:@{NSLocalizedDescriptionKey: @"Not granted permissions"}], nil);
                    self.postToWallBlock = nil;
                    self.postToWallText = nil;
                }
                if (self.shareToWallBlock) {
                    self.shareToWallBlock([NSError errorWithDomain:@"FBError" code:100000 userInfo:@{NSLocalizedDescriptionKey: @"Not granted permissions"}], nil);
                    self.shareToWallBlock = nil;
                    self.shareToWallText = nil;
                    self.shareToWallImage = nil;
                }
            }
            return;
        }
        [self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"What The Phuzzle?!");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
//                alertTitle = @"Session Error";
//                alertText = @"User cancelled login";
//                [self showMessage:alertText withTitle:alertTitle];
                [[NSNotificationCenter defaultCenter] postNotificationName:kFBNotificationLoggedCancel object:nil];
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
}

- (void) userLoggedIn
{
    NSLog(@"userLoggedIn");
    [[NSNotificationCenter defaultCenter] postNotificationName:kFBNotificationLoggedIn object:nil];
}
- (void) userLoggedOut
{
    NSLog(@"userLoggedOut");
    [[NSNotificationCenter defaultCenter] postNotificationName:kFBNotificationLoggedOut object:nil];
}

- (void) showMessage:(NSString*)alertText withTitle:(NSString*)alertTitle
{
    NSLog(@"showMessage:%@ withTitle:%@", alertText, alertTitle);
    [[[UIAlertView alloc] initWithTitle:alertTitle message:alertText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) loginWithCompletionBlock:(GWFBCompletionBlock) block
{
    // Open a session showing the user the login UI
    // You must ALWAYS ask for basic_info permissions when opening a session
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         [[GWFacebookManager sharedInstance] sessionStateChanged:session state:state error:error];
         if (!error && state == FBSessionStateOpen){
             block(nil, YES);
         }
         if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
             block(error, NO);
         }
     }];
}

- (void) logout
{
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (BOOL) isSessionOpen
{
    return FBSession.activeSession.state == FBSessionStateOpen;
}

- (BOOL) isSessionExtended
{
    return FBSession.activeSession.state == FBSessionStateOpenTokenExtended;
}

- (void) fetchFriendsWithCompletionBlock:(GWComletionBlock) block
{
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            block(nil, result);
        } else {
            block(error, nil);
        }
    }];
}

- (void) fetchSelfWithCompletionBlock:(GWComletionBlock) block
{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            block(nil, result);
        } else {
            block(error, nil);
        }
    }];
}

- (BOOL) checkPermissions
{
    NSArray * permissions = [[FBSession activeSession] permissions];
    NSLog(@"permissions: %@", permissions);
    
    for (NSString *perm in permissions) {
        if ([@"publish_actions" isEqualToString:perm]) {
            return YES;
        }
    }
    return NO;
}

- (void) requestPublishPermissionWithCompletionBlock:(GWFBCompletionBlock) block

{
    [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
        [[GWFacebookManager sharedInstance] sessionStateChanged:session state:state error:error];
        if (!error && state == FBSessionStateOpen){
            block(nil, YES);
        }
        if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
            block(error, NO);
        }
    }];
}

- (void) postToWall:(NSString*) text withCompletionBlock:(GWComletionBlock) block
{
    if (![self checkPermissions]) {
        self.postToWallBlock = block;
        self.postToWallText = text;
        [self requestPublishPermissionWithCompletionBlock:^(id error, BOOL success) {
            [self postToWall:text withCompletionBlock:block];
            self.postToWallBlock = nil;
            self.postToWallText = nil;
        }];
        return;
    }
    
    //TODO FACEBOOK share text to wall
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Who wants to Phuzzle me?", @"name",
                                   text, @"caption",
                                   @"What The Phuzzle? Turn Photos to Puzzles, And send to your Phuzzle buddies! Available now FREE on the iTunes App Store.", @"description",
                                   @"https://itunes.apple.com/us/app/what-the-phuzzle/id819213078?ls=1&mt=8", @"link",
                                   
                                   nil];
    
    // Make the request
    [FBRequestConnection startWithGraphPath:@"/me/feed"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  block(nil, nil);
                              } else {
                                  block(error, nil);
                              }
                          }];
}

- (void) sharedToWall:(NSString*) text withImage:(UIImage*)image withCompletionBlock:(GWComletionBlock) block
{
    if (![self checkPermissions]) {
        self.shareToWallBlock = block;
        self.shareToWallImage = image;
        self.shareToWallText = text;
        [self requestPublishPermissionWithCompletionBlock:^(id error, BOOL success) {
            [self sharedToWall:text withImage:image withCompletionBlock:block];
            self.shareToWallBlock = nil;
            self.shareToWallText = nil;
            self.shareToWallImage = nil;
        }];
        return;
    }
    
    //TODO FACEBOOK share result to wall
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   text, @"name",
                                   text, @"message",
                                   @"this is a description(sharing game result)", @"description",
                                   @"https://google.com", @"link",
                                   UIImageJPEGRepresentation(image, 90), @"source",
                                   nil];
    
    // Make the request
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  block(nil, nil);
                              } else {
                                  block(error, nil);
                              }
                          }];
}

+ (NSString *) errorMessage:(NSError*) fbError
{
    NSString *message = nil;
    if (fbError.userInfo[@"com.facebook.sdk:ParsedJSONResponseKey"]) {
        NSDictionary *body = fbError.userInfo[@"com.facebook.sdk:ParsedJSONResponseKey"][@"body"];
        NSDictionary *error = body[@"What The Phuzzle?!"];
        message = error[@"message"];
    }
    if (!message) {
        message = @"Something went wrong! Please try again.";
    }
    return  message;
}


@end
