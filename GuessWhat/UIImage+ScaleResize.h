//
// Created by Oleg on 3/5/14.
// Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (ScaleResize)
- (UIImage *) scaleToSize: (CGSize)size;
//- (UIImage *) scaleProportionalToSize: (CGSize)size1;
- (UIImage *) scaleProportionalToSize: (CGSize)size;
@end