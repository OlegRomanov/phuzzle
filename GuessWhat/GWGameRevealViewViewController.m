//
//  GWGameRevealViewViewController.m
//  Phuzzle
//
//  Created by Sunny on 2/24/14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWGameRevealViewViewController.h"
#import "GWPuzzle.h"

@interface GWGameRevealViewViewController ()

@end

@implementation GWGameRevealViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadAll];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadAll
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Loading";
    [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:self.puzzle.imageURL]
                                                        options:0
                                                       progress:^(NSUInteger receivedSize, long long expectedSize)
     {
         // progression tracking code
         hud.progress = receivedSize/expectedSize;
     }
                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
     {
         [hud hide:YES];
         if (image && finished)
         {
             
             self.puzzle.isStarted = @YES;
             
             //[self imageDidLoad:image];
             
         } else {
             [self.ivCorrect setImage: image];
             [self.lblAnswer setText:@"blah blah"];
             NSLog(@"Can't load image: %@", self.puzzle.imageURL);
         }
     }];
}

@end
