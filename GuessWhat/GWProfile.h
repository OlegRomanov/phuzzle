//
//  GWProfile.h
//  Phuzzle
//
//  Created by Artem Antipov on 26.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GWPuzzle;

@interface GWProfile : NSManagedObject

@property (nonatomic, retain) NSNumber * adClicks;
@property (nonatomic, retain) NSNumber * coins;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * facebookID;
@property (nonatomic, retain) NSNumber * higestConsecutiveWins;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * keys;
@property (nonatomic, retain) NSNumber * quizUnlocked;
@property (nonatomic, retain) NSNumber * referrals;
@property (nonatomic, retain) NSNumber * spyholeUnlocked;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * tilesUnlocked;
@property (nonatomic, retain) GWPuzzle *puzzle;
@property (nonatomic, retain) NSNumber *gameIdentify;
@end
