//
//  GWGameAlertView.m
//  Phuzzle
//
//  Created by Artem Antipov on 24.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWGameAlertView.h"
#import "UIImage+ScaleResize.h"


@interface GWGameAlertView ()
{
    NSArray *_products;
}
@end

#define IS_IPHONE_5 ([UIScreen mainScreen].bounds.size.height > 480.0)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation GWGameAlertView

+ (GWGameAlertView*) loseWithDelegate:(id<GWGameAlertViewDelegate>) delegate
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWGameAlertViewTimesUP" owner:self options:nil];
    
    GWGameAlertView *view = (GWGameAlertView*)[nib objectAtIndex:0];
    view.delegate = delegate;
    view.buySecondsButton.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:view selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:view selector:@selector(transactionFailed:) name:IAPHelperFailedTransactionNotification object:nil];
    
    if ([NSUserDefaults standardUserDefaults].seconds) {
        view.buySecondsButton.hidden = YES;
        return view;
    }
    
    [MBProgressHUD showHUDAddedTo:view animated:YES];
    [[GWStoreHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            view->_products = products;
            if (products.count > 0) {
                view.buySecondsButton.enabled = YES;
            } else {
                view.buySecondsButton.enabled = NO;
            }
        }
        [MBProgressHUD hideAllHUDsForView:view animated:YES];
    }];
    
    return  view;
}

+ (GWGameAlertView*) winWithDelegate:(id<GWGameAlertViewDelegate>) delegate withCoins:(NSNumber*) coins andWins:(NSNumber*) wins image:(UIImage *)img
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWGameAlertViewWin" owner:self options:nil];

    GWGameAlertView *view = (GWGameAlertView*)[nib objectAtIndex:0];
    view.delegate = delegate;

    view.winImageView.image = img;

//    view.winImageView
    if(IS_IPHONE_5 && !IS_IPAD){
view.winImageView.center = CGPointMake(view.center.x, 150);
    }

    
    UILabel *coinsLabel = (UILabel*)[view viewWithTag:1000];
    coinsLabel.text = [NSString stringWithFormat:@"%@", coins];
    
    UILabel *winsLabel = (UILabel*)[view viewWithTag:1001];
    winsLabel.text = [NSString stringWithFormat:@"%@", wins];
    
    return  view;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)yesPressed:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertView:self didPressed:YES];
    }
}

- (IBAction)revealPressed:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertViewDidPressReveal:self];
    }
}

- (IBAction)retryPressed:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertViewDidPressRepeat:self];
    }
}

- (IBAction)noPressed:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertView:self didPressed:NO];
    }
}
- (IBAction)sharePressed:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertViewDidPressShare:self];
    }
}

- (IBAction)saveImage:(id)sender {
    if (self.delegate) {
        [self.delegate gameAlertViewDidPressSave:self];
    }
}

- (IBAction)buySecondsPressed:(id)sender
{    
    self.buySecondsButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    SKProduct *product = _products[0];
    for (SKProduct *prod in _products) {
        if ([prod.productIdentifier isEqualToString:@"com.Whatthephuzzle.seconds30forever"]) {
            product = prod;
            break;
        }
    }

    NSLog(@"Покупаем %@...", product.productIdentifier);
    [[GWStoreHelper sharedInstance] buyProduct:product];
}

- (void)productPurchased:(NSNotification *)notification {
    
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
    SKPaymentTransaction * transaction = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:transaction.payment.productIdentifier]) {
            if ([@"com.Whatthephuzzle.keys3" isEqualToString:transaction.payment.productIdentifier]) {
            } else if ([@"com.Whatthephuzzle.seconds30forever" isEqualToString:transaction.payment.productIdentifier]) {
                self.buySecondsButton.hidden = YES;
            }
            
        }
    }];
}

- (void) transactionFailed:(NSNotification*) notification
{
    self.buySecondsButton.enabled = YES;
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
}



@end
