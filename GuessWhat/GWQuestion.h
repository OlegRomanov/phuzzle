//
//  GWQuestion.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GWPuzzle;

@interface GWQuestion : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSString * answers;
@property (nonatomic, retain) NSString * correctAnswer;
@property (nonatomic, retain) GWPuzzle *puzzle;

@end
