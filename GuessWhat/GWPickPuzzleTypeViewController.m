//
//  GWPickPuzzleTypeViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 26.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPickPuzzleTypeViewController.h"

#import "GWPuzzle+Methods.h"

#import "GWTilesGameViewController.h"
#import "GWSpyholeGameViewController.h"
#import "GWQuizGameViewController.h"
#import "GWMainViewController.h"


@interface GWPickPuzzleTypeViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation GWPickPuzzleTypeViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.noADS = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = CGSizeMake(320, 420);
}

- (IBAction)tilesPressed:(id)sender {
    GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    [self setPuzzle:kPuzzleTypeTiles];
}
- (IBAction)spyholePressed:(id)sender {
    GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    [self setPuzzle:kPuzzleTypeSpyhole];
}
- (IBAction)quizPressed:(id)sender {
    GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    [self setPuzzle:kPuzzleTypeQuiz];
}

- (IBAction)okPressed:(id)sender {
    GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    [self setPuzzle:kPuzzleTypeTiles];
}

- (void) setPuzzle:(NSString*) type
{
    [NSUserDefaults standardUserDefaults].devicePuzzleType = type;
}

- (IBAction)tilesPreview:(id)sender {
    GWTilesGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWTilesGameViewController"];
    vc.demoVersion = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)spyholePreview:(id)sender {
    GWSpyholeGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWSpyholeGameViewController"];
    vc.demoVersion = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)quizPreview:(id)sender {
    GWQuizGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWQuizGameViewController"];
    vc.demoVersion = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
