//
//  GWAnswerView.h
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GWAnswerView;
@protocol GWAnswerViewDelegate <NSObject>

- (void) answerView:(GWAnswerView*) view didPresseGuessWithAnswer:(NSString*) answer;
- (void) answerViewDidPressedKey:(GWAnswerView*) view;

@end

@interface GWAnswerView : UIView

+ (GWAnswerView*) viewWithDelegate:(id<GWAnswerViewDelegate>) delegate;

@property (nonatomic, strong) NSString *help;
@property (nonatomic , weak) id<GWAnswerViewDelegate> delegate;

@end
