//
//  GWTitleBar.m
//  Phuzzle
//
//  Created by Artem Antipov on 20.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWTitleBar.h"
#import "GWProfile+Methods.h"



@interface GWTitleBar ()
@property (strong, nonatomic) IBOutlet UIImageView *ngImage;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *coinsCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *keysCountLabel;

@property (strong, nonatomic) IBOutlet UIImageView *coinsIcon;
@property (strong, nonatomic) IBOutlet UIImageView *keysIcon;
@end

@implementation GWTitleBar

+ (GWTitleBar*) view
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWTitleBar" owner:self options:nil];
    
    GWTitleBar *view = (GWTitleBar*)[nib objectAtIndex:0];
    [view setup];
    
    return  view;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refresh)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:[NSManagedObjectContext MR_defaultContext]];
    
    GWProfile *profile = [GWProfile currentUser];
    
    self.usernameLabel.text = profile.username;
    self.coinsCountLabel.text = [NSString stringWithFormat:@" %@", profile.coins];
    self.keysCountLabel.text = [NSString stringWithFormat:@"%@ x", profile.keys];
}

- (void) refresh
{
    GWProfile *profile = [GWProfile currentUser];
    
    self.usernameLabel.text = profile.username;
    self.coinsCountLabel.text = [NSString stringWithFormat:@" %@", profile.coins];
    self.keysCountLabel.text = [NSString stringWithFormat:@"%@ x", profile.keys];
}


-(void)setBgImage:(UIImage *)bgImage
{
    self.ngImage.image = bgImage;
}

- (UIImage *)bgImage
{
    return self.ngImage.image;
}


@end
