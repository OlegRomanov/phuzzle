//
//  GWAudioPlayer.m
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWAudioPlayer.h"

#import <AVFoundation/AVFoundation.h>

@interface GWAudioPlayer ()
{
    CFURLRef soundFileURLRef;
    SystemSoundID  soundFileObject;
}

@end

@implementation GWAudioPlayer

+ (GWAudioPlayer*) playerWithDefaulClick
{
    GWAudioPlayer *player = [[GWAudioPlayer alloc] initWithSound:@"ButtonPop"];
    return player;
}

+ (GWAudioPlayer*) playerWithClockSound
{
    GWAudioPlayer *player = [[GWAudioPlayer alloc] initWithSound:@"Clock"];
    return player;
}

+ (GWAudioPlayer*) playerWithEarnKey
{
    GWAudioPlayer *player = [[GWAudioPlayer alloc] initWithSound:@"EarnKey"];
    return player;
}

+ (GWAudioPlayer*) playerWithHarp
{
    GWAudioPlayer *player = [[GWAudioPlayer alloc] initWithSound:@"Harp"];
    return player;
}

-(id)initWithSound:(NSString*) sound
{
    self = [super init];
    if (self)
    {
        
        CFBundleRef mainBundle = CFBundleGetMainBundle ();
        soundFileURLRef  = CFBundleCopyResourceURL (
                                                    mainBundle,
                                                    (CFStringRef)CFBridgingRetain(sound),
                                                    CFSTR("caf"),
                                                    NULL
                                                    );
        
        AudioServicesCreateSystemSoundID (
                                          soundFileURLRef,
                                          &soundFileObject
                                          );

    }
    return self;
}

-(void)playIfSoundisEnabled
{
//    if ([[NSUserDefaults standardUserDefaults] soundStatus]==YES)
//    {

        AudioServicesPlaySystemSound (soundFileObject);
//    }
}

- (void) stopSound
{
    AudioServicesDisposeSystemSoundID(soundFileObject);
}


@end
