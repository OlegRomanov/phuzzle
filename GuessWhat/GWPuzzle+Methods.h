//
//  GWPuzzle+Methods.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPuzzle.h"

extern NSString* const kPuzzleTypeTiles;
extern NSString* const kPuzzleTypeSpyhole;
extern NSString* const kPuzzleTypeQuiz;

@interface GWPuzzle (Methods)

+ (GWPuzzle*) createEntryForID:(NSNumber*) ID;
+ (GWPuzzle*) createWithDict:(NSDictionary*) dict;

- (NSDictionary*) toDictionary;


@end
