//
//  GWTilesPuzzleSendViewController.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"

@class GWProfile;

@interface GWTilesPuzzleSendViewController : GWBaseViewController
@property (nonatomic, strong) GWProfile *profile;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *whatIs;

@end
