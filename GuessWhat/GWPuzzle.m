//
//  GWPuzzle.m
//  Phuzzle
//
//  Created by Artem Antipov on 27.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPuzzle.h"
#import "GWProfile.h"
#import "GWQuestion.h"


@implementation GWPuzzle

@dynamic answer;
@dynamic coins;
@dynamic correct;
@dynamic id;
@dynamic imageURL;
@dynamic keySolved;
@dynamic my;
@dynamic order;
@dynamic resolved;
@dynamic type;
@dynamic isStarted;
@dynamic opponent;
@dynamic questions;

@end
