//
//  GWProfile+Methods.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWProfile.h"

@interface GWProfile (Methods)

+ (GWProfile*) currentUser;
+ (GWProfile*) createEntryForID:(NSNumber*) ID;
+ (GWProfile*) createWithDict:(NSDictionary*) dict;

@end
