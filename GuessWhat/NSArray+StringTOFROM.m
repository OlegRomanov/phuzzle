//
//  NSArray+StringTOFROM.m
//  HeartLeaf
//
//  Created by Artem Antipov on 04.11.13.
//  Copyright (c) 2013 Nicecode. All rights reserved.
//

#import "NSArray+StringTOFROM.h"

@implementation NSArray (StringTOFROM)

+ (NSArray*) arrayStringsWithString:(NSString*)string
{
    NSArray *compo = [string componentsSeparatedByString:@","];
    NSMutableArray *ma = [NSMutableArray arrayWithArray:compo];
    while([[ma lastObject] isEqualToString:@""]) {
        [ma removeLastObject];
    }
    return ma;
}

+ (NSArray*) arrayNumbersWithString:(NSString*)string
{
    NSArray *compo = [string componentsSeparatedByString:@","];
    NSMutableArray *ma = [NSMutableArray arrayWithArray:compo];
    while([[ma lastObject] isEqualToString:@""]) {
        [ma removeLastObject];
    }
    NSMutableArray *am = [NSMutableArray array];
    for (NSString *string in ma) {
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *number = [f numberFromString:string];
        [am addObject:number];
    }
    return am;
}

+ (NSArray*) arrayWithString:(NSString*)string
{
    NSArray *compo = [string componentsSeparatedByString:@","];
    NSMutableArray *ma = [NSMutableArray arrayWithArray:compo];
    while([[ma lastObject] isEqualToString:@""]) {
        [ma removeLastObject];
    }
    return ma;
}

- (NSString*) toString
{
    NSString *strings = @"";
    for (id string in self) {
        strings = [strings stringByAppendingString:[string description]];
        if (![string isEqual:[self lastObject]]) {
            strings = [strings stringByAppendingString:@","];
        }
    }
    return strings;
}

@end
