//
//  GWGameAlertView.h
//  Phuzzle
//
//  Created by Artem Antipov on 24.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//


// coins 1000
// wins in row 1001
#import <UIKit/UIKit.h>

@class GWGameAlertView;

@protocol GWGameAlertViewDelegate <NSObject>

-(void) gameAlertView:(GWGameAlertView*) view didPressed:(BOOL) continueGame;

-(void) gameAlertViewDidPressReveal:(GWGameAlertView*) view;
-(void) gameAlertViewDidPressRepeat:(GWGameAlertView*) view;
-(void) gameAlertViewDeduct1Key:(GWGameAlertView*) view;

- (void) gameAlertViewDidPressShare:(GWGameAlertView*) view;
- (void) gameAlertViewDidPressSave:(GWGameAlertView*) view;
- (void) gameAlertViewDidPressedKey:(GWGameAlertView*) view;



@end

@interface GWGameAlertView : UIView

@property (nonatomic, retain) GWPuzzle *puzzle;

@property (strong, nonatomic) IBOutlet UIButton *buySecondsButton;
@property (strong, nonatomic) IBOutlet UIImageView *winImageView;
@property (nonatomic, weak) id<GWGameAlertViewDelegate> delegate;

+ (GWGameAlertView*) loseWithDelegate:(id<GWGameAlertViewDelegate>) delegate;
+ (GWGameAlertView*) winWithDelegate:(id<GWGameAlertViewDelegate>) delegate withCoins:(NSNumber*) coins andWins:(NSNumber*) wins image:(UIImage *)img;

@end
