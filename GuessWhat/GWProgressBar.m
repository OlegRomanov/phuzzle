//
//  GWProgressBar.m
//  Phuzzle
//
//  Created by Artem Antipov on 26.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWProgressBar.h"

@interface GWProgressBar ()
{
    CGFloat _progress;
}
@property (strong, nonatomic) IBOutlet UIImageView *leftImage;
@property (strong, nonatomic) IBOutlet UIImageView *progressImage;
@property (strong, nonatomic) IBOutlet UIImageView *rightImage;

@end

@implementation GWProgressBar

+ (GWProgressBar*) view
{
    return [GWProgressBar viewWithProgress:0.5];
}

+ (GWProgressBar*) viewWithProgress:(CGFloat) progress
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWProgressBar" owner:self options:nil];
    
    GWProgressBar *view = (GWProgressBar*)[nib objectAtIndex:0];
    [view setProgress: progress];
    
    return  view;
}

-(void) setProgress:(CGFloat) progress
{
    if (progress > 1.0) {
        progress = 1.0;
    } else if (progress < 0) {
        progress = 0.0;
        self.leftImage.hidden = YES;
        self.rightImage.hidden = YES;
        self.progressImage.hidden = YES;
        return;
    }
    
    self.leftImage.hidden = NO;
    self.rightImage.hidden = NO;
    self.progressImage.hidden = NO;
    
    CGFloat allWidth = self.frame.size.width-37;
    CGFloat width = allWidth*progress;
    
    CGRect frame = self.progressImage.frame;
    frame.size.width = width;
    self.progressImage.frame = frame;
    
    CGRect frame1 = self.rightImage.frame;
    frame1.origin.x = frame.origin.x + frame.size.width;
    self.rightImage.frame = frame1;
    
    _progress = progress;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setProgress:_progress];
}


@end
