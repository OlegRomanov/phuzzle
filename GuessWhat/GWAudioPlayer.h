//
//  GWAudioPlayer.h
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWAudioPlayer : NSObject

+ (GWAudioPlayer*) playerWithDefaulClick;

+ (GWAudioPlayer*) playerWithClockSound;
+ (GWAudioPlayer*) playerWithEarnKey;
+ (GWAudioPlayer*) playerWithHarp;

-(id)initWithSound:(NSString*) sound;

-(void)playIfSoundisEnabled;
-(void) stopSound;

@end
