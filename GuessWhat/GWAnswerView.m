//
//  GWAnswerView.m
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWAnswerView.h"

@interface GWAnswerView ()
<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *guessButton;

@property (strong, nonatomic) IBOutlet UITextField *answerField;
@end

@implementation GWAnswerView


+ (GWAnswerView*) viewWithDelegate:(id<GWAnswerViewDelegate>) delegate
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWAnswerView" owner:self options:nil];
    
    GWAnswerView *view = (GWAnswerView*)[nib objectAtIndex:0];
    view.delegate = delegate;
    view.answerField.delegate = view;
    
    return  view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)guessPressed:(id)sender {
    if (self.delegate) {
        [self.delegate answerView:self didPresseGuessWithAnswer:self.answerField.text];
    }
}
- (IBAction)keyPressed:(id)sender {
    if (self.delegate) {
        [self.delegate answerViewDidPressedKey:self];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.text.length > 0) {
        [self guessPressed:textField];
    }
    
    return YES;
}

- (void)setHelp:(NSString *)help
{
    _help = help;
    
    self.answerField.text = help;
    self.answerField.userInteractionEnabled = NO;
    CGRect frame = self.answerField.frame;
    frame.size.width = [UIApplication currentSize].width - frame.origin.x - 10;
    self.answerField.frame = frame;
    self.guessButton.hidden = YES;
}

- (BOOL)resignFirstResponder
{
    self.answerField.text = @"";
    return [self.answerField resignFirstResponder];
}

- (BOOL)becomeFirstResponder
{
    return [self.answerField becomeFirstResponder];
}

@end
