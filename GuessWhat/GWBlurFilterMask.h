//
//  GWBlurFilterMask.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface GWBlurFilterMask : CALayer

@property (assign) CGPoint origin;
@property (assign) CGFloat diameter;

@end
