//
//  GWButtonsSingltone.m
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWButtonsSingltone.h"

@implementation GWButtonsSingltone

+ (GWButtonsSingltone*)sharedInstance
{
    static dispatch_once_t once;
    static GWButtonsSingltone* sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.buttonsMute = [NSMutableDictionary dictionary];
        self.buttonsSound = [NSMutableDictionary dictionary];
    }
    return self;
}

@end
