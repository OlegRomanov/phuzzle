//
//  GWDataLoader.h
//  Phuzzle
//
//  Created by Artem Antipov on 20.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kNotificationRecivePuzzle;

typedef void(^GWProgressBlock)(long long totalBytesWritten, long long totalBytesExpectedToWrite);
typedef void(^GWComletionBlock)(id error, id result);

@class GWPuzzle;

@interface GWDataLoader : NSObject

@property (nonatomic, readonly) BOOL internetConnected;

+ (GWDataLoader*)sharedInstance;

+ (UIAlertView*) alertViewForError:(NSError*) error;

- (void) loginWithUsername:(NSString*) username withPassword:(NSString*)pass andCompletinoBlock:(GWComletionBlock) block;

- (void) registerWithUsername:(NSString*) username withPassword:(NSString*)pass withEmail:(NSString*)email andCompletinoBlock:(GWComletionBlock) block;

- (void) logout;


- (void) uploadPuzzle:(GWPuzzle*) puzzle withImage:(UIImage*) image withProgressBlock:(GWProgressBlock) progessBlock andCompletinoBlock:(GWComletionBlock) block;

- (void) loadPuzzleWithID:(NSNumber*)puzzleID withCompletionBlock:(GWComletionBlock) block;

- (void) loadAllPuzzlesWithCompletionBlock:(GWComletionBlock) block;

- (void) loadAllGamesWithCompletionBlock:(GWComletionBlock) block;

- (void) sendPuzzleRezult:(GWPuzzle*) puzzle andCompletinoBlock:(GWComletionBlock) block;

- (void) sendInviteToEmail:(NSString*) email withCompletionBlock:(GWComletionBlock) block;

- (void) checkFBFriendsIDs:(NSArray*) ids withCompletionBlock:(GWComletionBlock) block;

- (void) loginWithFacebook:(NSString*)username withID:(NSNumber*)fbID andCompletinoBlock:(GWComletionBlock) block;
- (void) sendInviteToFacebook:(NSNumber*) fbID withCompletionBlock:(GWComletionBlock) block;

- (void) buyGameType:(NSString*) gameType withCompletionBlock:(GWComletionBlock) block;

- (void) registerDeviceToken;
- (void) unregisterDeviceToken;

- (void) adsClick;
- (void) buyKeysWithCompletionBlock:(GWComletionBlock) block;
- (void) getKeyWithCompletionBlock:(GWComletionBlock) block;

- (void) getKeys:(NSNumber*)count WithCompletionBlock:(GWComletionBlock) block;

- (void) loadQuestoinsWithCompletionBlock:(GWComletionBlock) block;
- (void) deleteGame:(NSNumber*) gameID withCompletionBlock:(GWComletionBlock) block;
- (void)loadRandomPlayerWithCompletionBlock:(GWComletionBlock)block;
@end
