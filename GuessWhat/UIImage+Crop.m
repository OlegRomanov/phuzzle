//
//  UIImage+Crop.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "UIImage+Crop.h"

@implementation UIImage (Crop)

- (UIImage *)imageCroppedWithRect:(CGRect)rect
{
    if (self.scale > 1.0f) {    // this is for Retina display capability
        rect = CGRectMake(rect.origin.x * self.scale,
                          rect.origin.y * self.scale,
                          rect.size.width * self.scale,
                          rect.size.height * self.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

- (NSArray*) splitImage
{
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:9];
    UIImage *bigImage = self;
    float bigWidth = bigImage.size.width;
    float bigHeight = bigImage.size.height;
    
    UIImageOrientation orientation = bigImage.imageOrientation;
    float width = bigImage.size.width / 3;
    float height = bigImage.size.height / 3;
    CGRect frame = CGRectMake(0.0, 0.0, width, height);
    if (orientation == UIImageOrientationLeft || orientation == UIImageOrientationRight) {
        width = bigImage.size.height / 3;
        height = bigImage.size.width / 3;
        frame = CGRectMake(0.0, bigWidth - height, width, height);
    } else if (orientation == UIImageOrientationDown) {
        frame = CGRectMake(bigWidth - width, bigHeight - height, width, height);
    }
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            UIImage *piece = [bigImage imageCroppedWithRect:frame];
            if (orientation == UIImageOrientationLeft || orientation == UIImageOrientationRight) {
                frame.origin.y -= frame.size.height;
            } else if (orientation == UIImageOrientationDown) {
                frame.origin.x -= frame.size.width;
            } else {
                frame.origin.x += frame.size.width;
            }
            if (!piece) {
                return [NSArray array];
            }
            [images addObject:piece];
        }
        if (orientation == UIImageOrientationLeft || orientation == UIImageOrientationRight) {
            frame.origin.y = bigWidth - height;
            frame.origin.x += frame.size.width;
        } else if (orientation == UIImageOrientationDown) {
            frame.origin.x = bigWidth - width;
            frame.origin.y -= frame.size.height;
        } else {
            frame.origin.x = 0.0;
            frame.origin.y += frame.size.height;
        }
    }
    return images;
}

#pragma mark Scale and crop image

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize
{
    UIImage *sourceImage = self;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
