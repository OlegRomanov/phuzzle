//
//  GWInviteViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWInviteViewController.h"

#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWProfile+Methods.h"

@interface GWInviteViewController ()
<UITextFieldDelegate, GWCameraViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet GWTextField *emailField;
@end

@implementation GWInviteViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = self.scrollView.frame.size;
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //    double duration = [number doubleValue];
    
    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom + size.height, self.scrollView.contentInset.right)];
}

-(void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //    double duration = [number doubleValue];
    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom - size.height , self.scrollView.contentInset.right)];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self invitePressed:textField];
    return NO;
}

-(BOOL) validate
{
    if (self.emailField.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please Enter an Email Address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return NO;
    } else if ([[self.emailField.text uppercaseString] isEqualToString:[[GWProfile currentUser].email uppercaseString]]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You can not send an email invite to yourself!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    return YES;
}

- (IBAction)userNameInvitePressed:(id)sender {
    [self dismissKeyboard];
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[GWDataLoader sharedInstance] loadRandomPlayerWithCompletionBlock:^(id error, id result) {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
            if (result) {
                // start new game
                [self createPuzzleWithProfile:result];
            } else {
                [[[GWAudioPlayer alloc] initWithSound:@"InviteSent"]playIfSoundisEnabled];
                //show invite sent
                UIView * view = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
                view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
                view.alpha = 0.0;
                [self.navigationController.view addSubview:view];

                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 150)];
                image.center = view.center;
                image.image = [UIImage imageNamed:@"INVITE SENT"];
                image.userInteractionEnabled = NO;
                [view addSubview:image];

                [UIView animateWithDuration:0.15 animations:^{
                    view.alpha = 2.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.25 delay:1 options:0 animations:^{
                        view.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [view removeFromSuperview];
                    }];
                }];

                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                        initWithTarget:self
                                action:@selector(dismissPopup:)];

                [view addGestureRecognizer:tap];

            }
        }
    }];
}

- (IBAction)invitePressed:(id)sender {
    [self dismissKeyboard];
    if (![self validate]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[GWDataLoader sharedInstance] sendInviteToEmail:self.emailField.text withCompletionBlock:^(id error, id result) {
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
            if (result) {
                // start new game
                [self createPuzzleWithProfile:result];
            } else {
                [[[GWAudioPlayer alloc] initWithSound:@"InviteSent"]playIfSoundisEnabled];
                //show invite sent
                UIView * view = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
                view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
                view.alpha = 0.0;
                [self.navigationController.view addSubview:view];
                
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 400, 150)];
                image.center = view.center;
                image.image = [UIImage imageNamed:@"INVITE SENT"];
                image.userInteractionEnabled = NO;
                [view addSubview:image];
                
                [UIView animateWithDuration:0.15 animations:^{
                    view.alpha = 2.0;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.25 delay:1 options:0 animations:^{
                        view.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [view removeFromSuperview];
                    }];
                }];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(dismissPopup:)];
                
                [view addGestureRecognizer:tap];
                
            }
        }
    }];
    
}

- (void)createPuzzleWithProfile:(GWProfile*) profile {
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"NavControllerGWCameraViewController"];
    GWCameraViewController *vc = (GWCameraViewController*)nc.topViewController;
    vc.delegate = self;
    vc.profile = profile;
    
    [self presentViewController:nc animated:YES completion:nil];
}

-(void)didCapturedImage:(UIImage *)image withName:(NSString *)text toProfile:(GWProfile *)profile
{
    GWPuzzleTypeSelectViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleTypeSelectViewController"];
    vc.image = image;
    vc.whatIs = text;
    vc.profile = profile;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)captureImageDidDissmis
{
}

-(void)captureImageWillDissmis
{
}

- (void) dismissPopup:(UITapGestureRecognizer*) tap
{
    UIView *view = tap.view;
    
    [UIView animateWithDuration:0.15 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}


- (IBAction)findPressed:(id)sender {
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
