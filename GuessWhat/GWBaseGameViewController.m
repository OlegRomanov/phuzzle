//
//  GWBaseGameViewController.m
//  Phuzzle
//
//  Created by Artem Antipov on 27.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseGameViewController.h"

#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

#import "GWAnswerView.h"
#import "GWGameTimerView.h"

#import "GWGameAlertView.h"
#import "GWGameRevealViewViewController.h"
#import "CustomIOS7AlertView.h"

#define GWREVEALCOIN 50

@interface GWBaseGameViewController ()
<GWAnswerViewDelegate, GWGameTimerViewDelegate, GWCameraViewControllerDelegate, GWGameAlertViewDelegate,UIAlertViewDelegate>
{
    BOOL _isActive;
    BOOL _isLoad;
    BOOL _sharing;
    BOOL revealed;
    NSNumber *_coins;
    
    NSArray *_products;
    
    UIView *gameAlertView;
    
    
}

@end

@implementation GWBaseGameViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.noADS = YES;
    self.notRecivePushes = YES;
}


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.profile = self.puzzle.opponent;
    
    self.answerView = [GWAnswerView viewWithDelegate:self];
    
    self.timerView = [GWGameTimerView viewWithDelegate:self];
    
        if (![self.puzzle.type isEqualToString:kPuzzleTypeQuiz]) {
          [self loadImage];
        }
    revealed=NO;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isLoad) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    } else {
        _isLoad = YES;
    }
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(shareToWall)
                                                 name:kFBNotificationLoggedIn
                                               object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transactionFailed:) name:IAPHelperFailedTransactionNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.timerView stopTimer];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedIn
                                                  object:nil];
    //
    //     [[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                     name:IAPHelperProductPurchasedNotification
    //                                                   object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                    name:IAPHelperFailedTransactionNotification
    //                                                  object:nil];
}

-(void)keyboardWillHide:(NSNotification*)notification
{
    @throw @"No implemeted keyboardWillHide";
}

-(void)keyboardWillShow:(NSNotification*)notification
{
    @throw @"No implemeted keyboardWillShow";
}

#pragma mark - load image
- (void) loadImage
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Loading";
    [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:self.puzzle.imageURL]
                                                        options:0
                                                       progress:^(NSUInteger receivedSize, long long expectedSize)
     {
         // progression tracking code
         hud.progress = receivedSize/expectedSize;
     }
                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
     {
         [hud hide:YES];
         if (image && finished)
         {
             _isActive = YES;
             self.puzzle.isStarted = @YES;
             [self imageDidLoad:image];
             
         } else {
             
             NSLog(@"Can't load image: %@", self.puzzle.imageURL);
         }
     }];
}

- (void) imageDidLoad:(UIImage*) image
{
    self.image=image;
   // @throw @"No implemeted imageDidLoad";
}

#pragma mark - start new game

- (void) startNewGame
{
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"NavControllerGWCameraViewController"];
    GWCameraViewController *vc = (GWCameraViewController*)nc.topViewController;
    vc.delegate = self;
    vc.profile = self.profile;
    [self presentViewController:nc animated:YES completion:nil];
}

-(void)didCapturedImage:(UIImage *)image withName:(NSString *)text toProfile:(GWProfile *)profile
{
    GWPuzzleTypeSelectViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleTypeSelectViewController"];
    vc.image = image;
    vc.whatIs = text;
    vc.profile = profile;
    
    NSArray *vcs = @[self.navigationController.viewControllers[0], vc];
    [self.navigationController setViewControllers:vcs animated:YES];
    
    
}

-(void)captureImageDidDissmis
{
}

-(void)captureImageWillDissmis
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - game win/lose

- (void) gameDidEnd:(BOOL) correct
{
}

- (void) showKeyAddedInView:(UIView*) superview
{
    [[[GWAudioPlayer alloc] initWithSound:@"EarnKey"]playIfSoundisEnabled];
    UIView * view = [[UIView alloc] initWithFrame:self.view.frame];
    view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    view.alpha = 0.0;
    [superview addSubview:view];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 306, 290)];
    image.center = view.center;
    image.image = [UIImage imageNamed:@"1KEYMESSAGE"];
    image.userInteractionEnabled = NO;
    [view addSubview:image];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:2 options:0 animations:^{
            view.alpha = 0.0;
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
            [self showADS];
        }];
    }];
}

- (void) gameWin
{

    [[[GWAudioPlayer alloc] initWithSound:@"PuzzleSolved"]playIfSoundisEnabled];
    if (self.demoVersion) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    _isActive = NO;
    
    self.puzzle.resolved = @YES;
    self.puzzle.correct = @YES;
    _coins = @([self.timerView stopTimer]);
    self.puzzle.coins = [_coins copy];
    
    NSInteger currentCount = [GWProfile currentUser].coins.integerValue;
    NSInteger winCount = _coins.integerValue;
    
    NSInteger before = currentCount/100;
    NSInteger after = (currentCount+winCount)/100;
    
    
    GWGameAlertView *alert = [GWGameAlertView winWithDelegate:self withCoins:_coins andWins:@(self.puzzle.opponent.higestConsecutiveWins.longLongValue+1) image:(UIImage *)self.image];
    alert.frame = self.view.frame;
    alert.alpha = 0.0;
    [self.view addSubview:alert];
    
    [UIView animateWithDuration:0.35 delay:0.0 options:0 animations:^{
        alert.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (after-before >= 1) {
            [self showKeyAddedInView:alert];
        } else {
            [self showADS];
        }
        [self gameDidEnd:YES];
    }];
}

- (void) showADS
{
    RevMobFullscreen *ad = [[RevMobAds session] fullscreen]; // you must retain this object
    [ad loadWithSuccessHandler:^(RevMobFullscreen *fs) {
        [fs showAd];
        NSLog(@"Ad loaded");
    } andLoadFailHandler:^(RevMobFullscreen *fs, NSError *error) {
        NSLog(@"Ad error: %@",error);
    } onClickHandler:^{
        NSLog(@"Ad clicked");
        [[GWDataLoader sharedInstance] adsClick];
    } onCloseHandler:^{
        NSLog(@"Ad closed");
    }];
}

- (void) gameLoose
{
    [[[GWAudioPlayer alloc] initWithSound:@"TooSlow"]playIfSoundisEnabled];
    if (self.demoVersion) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    _isActive = NO;
    
    self.puzzle.resolved = @YES;
    self.puzzle.correct = @NO;
   // self.puzzle.coins = @0;
    
    [self.timerView stopTimer];
    
    GWGameAlertView *alert = [GWGameAlertView loseWithDelegate:self];
    alert.puzzle=self.puzzle;
    alert.frame = self.view.frame;
    alert.alpha = 0.0;
    [self.view addSubview:alert];
    
    [UIView animateWithDuration:0.35 animations:^{
        alert.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self gameDidEnd:NO];
    }];
}

#pragma mark - answerView

-(void)answerViewDidPressedKey:(GWAnswerView *)view
{
    if ([GWProfile currentUser].keys.integerValue > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure yo want to use 1 key to solve this Phuzzle?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 101;
        [alert show];
    }
}

-(void)answerView:(GWAnswerView *)view didPresseGuessWithAnswer:(NSString *)answer
{
    [view resignFirstResponder];
    if ([[answer uppercaseString] isEqualToString:[self.puzzle.answer uppercaseString]]) {
        [self gameWin];
    } else {
        [[[GWAudioPlayer alloc] initWithSound:@"Quack"]playIfSoundisEnabled];
        [self showGuessAgainView];
    }
}


#pragma mark- show rightanswer
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    
    
    [[GWDataLoader sharedInstance] sendPuzzleRezult:self.puzzle andCompletinoBlock:^(id error, id result) {}];
    
    if (buttonIndex==0) {
        [alertView close];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } else {
        [alertView close];
         [self gameDidEnd:NO];
        [self startNewGame];
    }
    
   
    
}
- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
    demoView.backgroundColor=[CustomIOS7AlertView colorFromHexString:@"000000"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(00, -10, 320, 410)];
    [imageView setImage:self.image];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [demoView addSubview:imageView];
    UILabel *answer = [[UILabel alloc] initWithFrame:CGRectMake((demoView.bounds.size.width - 250) / 2, demoView.bounds.size.height- 30, 250, 30)];
    [answer setTextAlignment:NSTextAlignmentCenter];
    answer.text=self.puzzle.answer;
    answer.font = [UIFont defaultFontWithSize:34.0f];
    answer.textColor=[CustomIOS7AlertView colorFromHexString:@"#FFFFFF"];
    [demoView addSubview:answer];
    return demoView;
}


#pragma mark - gameTimer

-(void) gameTimerViewDidEnded:(GWGameTimerView *)view
{
    [self gameLoose];
}

-(BOOL)gameTimerViewGameActive:(GWGameTimerView *)view
{
    return _isActive;
}

-(void)gameTimerViewDidBakcPressed:(GWGameTimerView *)view
{
    if (self.demoVersion) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Leave Phuzzle?" message:@"You will forfeit the game, and lose your wins in-a-row!" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag = 100;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    if (alertView.tag == 100) {
        switch (buttonIndex) {
            case 1:
            {
                self.puzzle.resolved = @YES;
                self.puzzle.correct = @NO;
                if (self.puzzle.coins==nil) {
                    self.puzzle.coins = @0;
                }
                
                
                [self.timerView stopTimer];
                [[GWDataLoader sharedInstance] sendPuzzleRezult:self.puzzle andCompletinoBlock:^(id error, id result) {}];
                [self gameDidEnd:NO];
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
            }
            case 0:
                break;
            default:
                break;
        }
    }else if (alertView.tag == 101) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                self.puzzle.keySolved = @YES;
                [self gameWin];
                break;
            default:
                break;
        }
    }else if (alertView.tag==303) {
        if (buttonIndex==1) {
            CustomIOS7AlertView *alertView2 = [[CustomIOS7AlertView alloc] init];
            
            // Add some custom content to the alert view
            [alertView2 setContainerView:[self createDemoView]];
            
            // Modify the parameters
            [alertView2 setButtonTitles:[NSMutableArray arrayWithObjects:@"homeREV",@"PLAYAGAIN", nil]];
            [alertView2 setDelegate:self];
            
            // You may use a Block, rather than a delegate.
            
            
            [alertView2 setUseMotionEffects:true];
            int cur=self.puzzle.coins.intValue;
            cur=cur - 50;
            self.puzzle.coins=[NSNumber numberWithInt:cur];
            
            [alertView2 show];
        }
    }else if (alertView.tag == 2){
        if (buttonIndex == 0){
            [self playPuzzle:self.puzzle];
        }
    }
    
}

#pragma mark - gameAlert

- (void) showGuessAgainView
{
    UIView * view = [[UIView alloc] initWithFrame:self.view.frame];
    view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    view.alpha = 0.0;
    [self.view addSubview:view];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 280, 300)];
    image.center = view.center;
    image.image = [UIImage imageNamed:@"GUESSAGAIN"];
    image.userInteractionEnabled = NO;
    [view addSubview:image];
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 delay:0.25 options:0 animations:^{
            view.alpha = 0.0;
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }];

}

-(void) gameAlertViewDidPressReveal:(GWGameAlertView*) view {
    
    
   // self.puzzle.coins=[NSNumber numberWithInt:300];
    
    // And launch the dialog
     NSInteger currentCount = [GWProfile currentUser].coins.integerValue;
    if (currentCount>=50) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Use 50 coins to see this photo?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.tag = 303;
        [alert show];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You need 50 coins to see this photo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 304;
        [alert show];
    }
    
    //[self.view addSubview:alert.view];
}
-(void) gameAlertViewDeduct1Key:(GWGameAlertView*) view{
    
}

-(void)gameAlertView:(GWGameAlertView *)view didPressed:(BOOL)continueGame
{
    
    
    [UIView animateWithDuration:0.35 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
    
    [[GWDataLoader sharedInstance] sendPuzzleRezult:self.puzzle andCompletinoBlock:^(id error, id result) {}];
    
    if (continueGame) {
        [self startNewGame];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    [self.puzzle MR_deleteEntity];
}

- (void)gameAlertViewDidPressShare:(GWGameAlertView *)view
{
    
    if ([GWFacebookManager sharedInstance].isSessionOpen || [GWFacebookManager sharedInstance].isSessionExtended) {
        [self shareToWall];
    } else {
        [[GWFacebookManager sharedInstance] loginWithCompletionBlock:^(id error, BOOL success) {
        }];
    }
    
    
}

- (void) shareToWall
{
    if (_sharing) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _sharing = YES;
    //TODO FACEBOOK Share game result text
    NSString * shareText = [NSString stringWithFormat:@"Just won %@ coins by solving this Phuzzle! Think you can beat that score?",_coins];
    [[GWFacebookManager sharedInstance] sharedToWall:shareText  withImage:self.image withCompletionBlock:^(NSError* error, id result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (!_sharing) {
            return ;
        }
        
        if (!error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Your win was successfuly shared to your Facebook wall" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        } else {
            NSString * alertText = error.localizedDescription;
            if (error.code != 10000) {
                alertText = [FBErrorUtility userMessageForError:error];
                if (!alertText) {
                    alertText = [GWFacebookManager errorMessage:error];
                }
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:alertText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
        _sharing = NO;
    }];
}

-(void)gameAlertViewDidPressSave:(GWGameAlertView *)view
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UIImageWriteToSavedPhotosAlbum(self.image, self, @selector(savedImage:didFinishSavingWithError:contextInfo:), nil);
}

- (void) savedImage:(UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo:(void *) contextInfo
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if (error) {
        [[[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        [[[UIAlertView alloc]initWithTitle:nil message:@"Saved To Photo Album" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}
-(void)gameAlertViewDidPressRepeat:(GWGameAlertView *)view{
    GWProfile *profile = [GWProfile currentUser];
    int myCoins = [profile.coins intValue];
    if (myCoins >= 25){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to use 25 coins to retry this Phuzzle?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO",nil];
        alert.delegate = self;
        alert.tag = 2;
        [alert show];

    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"You need 25 coins to retry. You only have %d.",myCoins] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

//-(void)gameAlertViewDidPressRepeate:(GWGameAlertView *)view
//{
//    gameAlertView = view;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    SKProduct *product = view.products[0];
//    for (SKProduct *prod in view.products) {
//        if ([prod.productIdentifier isEqualToString:@"com.Phuzzle.seconds30"]) {
//            product = prod;
//            break;
//        }
//    }
//
//    NSLog(@"Покупаем %@...", product.productIdentifier);
//    [[GWStoreHelper sharedInstance] buyProduct:product];
//
//}
//
//- (void)productPurchased:(NSNotification *)notification {
//
//    SKPaymentTransaction * transaction = notification.object;
//    if ([@"com.Phuzzle.seconds30" isEqualToString:transaction.payment.productIdentifier]) {
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        _isActive = YES;
//        self.puzzle.isStarted = @YES;
//        [self.timerView startTimer:20];
//        [gameAlertView removeFromSuperview];
//    }
//
//}
//
//- (void) transactionFailed:(NSNotification*) notification
//{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//}
//#pragma mark -


@end
