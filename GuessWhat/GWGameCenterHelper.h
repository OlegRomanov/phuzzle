//
//  GWGameCenterHelper.h
//  Phuzzle
//
//  Created by Artem Antipov on 10.02.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSString *const GWAchiviment100Keys;

UIKIT_EXTERN NSString *const GWGameCenterAchiveLoadedNotification;

@interface GWGameCenterHelper : NSObject

+ (GWGameCenterHelper *)sharedInstance;

- (void) loadAchievements;
- (GKAchievement*) getAchievementForIdentifier: (NSString*) identifier;
- (void) reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent;

@end
