//
//  GWProgressBar.h
//  Phuzzle
//
//  Created by Artem Antipov on 26.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWProgressBar : UIView

+ (GWProgressBar*) view;
+ (GWProgressBar*) viewWithProgress:(CGFloat) progress;

-(void) setProgress:(CGFloat) progress;

@end
