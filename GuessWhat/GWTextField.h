//
//  GWTextField.h
//  Phuzzle
//
//  Created by Artem Antipov on 22.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWTextField : UITextField

@property (nonatomic, readwrite) float verticalPadding;
@property (nonatomic, readwrite) float horizontalPadding;

@end
