//
//  GWPuzzleTypeSelectViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPuzzleTypeSelectViewController.h"
#import "GWTilesPuzzleSendViewController.h"
#import "GWPuzzleSendViewController.h"
#import "GWPuzzleSentViewController.h"

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

#import <QuartzCore/QuartzCore.h>

@interface GWPuzzleTypeSelectViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIButton *tilesButton;
@property (strong, nonatomic) IBOutlet UIButton *spyholeButton;
@property (strong, nonatomic) IBOutlet UIButton *quizButton;

@property (strong, nonatomic) IBOutlet UIButton *spyholeUnlockButton;
@property (strong, nonatomic) IBOutlet UIButton *quizUnlockButton;
@property (strong, nonatomic) IBOutlet UIButton *tilesUnlockButton;

@end

@implementation GWPuzzleTypeSelectViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tittleBarHidden = NO;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.imageView.image = self.image;
    self.imageView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0.302 alpha:1.0].CGColor;
    self.imageView.layer.borderWidth = 2.0;
    self.imageView.layer.cornerRadius = 15.0;
    
    GWProfile *profile = [GWProfile currentUser];
    
    NSString* game = [NSUserDefaults standardUserDefaults].devicePuzzleType;
    
    profile.quizUnlocked = @YES;
    profile.spyholeUnlocked = @YES;
    profile.tilesUnlocked = @YES;
    
    if (profile.quizUnlocked.boolValue) {
        self.quizButton.enabled = YES;
        CGRect frame = self.quizButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.quizButton.frame = frame;
        self.quizUnlockButton.hidden = YES;
    } else {
        self.spyholeButton.enabled = NO;
        self.spyholeUnlockButton.hidden = NO;
    }
    
    if (profile.spyholeUnlocked.boolValue) {
        self.spyholeButton.enabled = YES;
        CGRect frame = self.spyholeButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.spyholeButton.frame = frame;
        self.spyholeUnlockButton.hidden = YES;
    } else {
        self.spyholeButton.enabled = NO;
        self.spyholeUnlockButton.hidden = NO;
    }
    
    if (profile.tilesUnlocked.boolValue) {
        self.tilesButton.enabled = YES;
        CGRect frame = self.tilesButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.tilesButton.frame = frame;
        self.tilesUnlockButton.hidden = YES;
    } else {
        self.tilesButton.enabled = NO;
        self.tilesUnlockButton.hidden = NO;
    }
    
//    if (DEBUG) {
//        self.tilesButton.enabled = YES;
//        self.tilesUnlockButton.hidden = YES;
//        self.spyholeButton.enabled = YES;
//        self.spyholeUnlockButton.hidden = YES;
//        self.quizButton.enabled = YES;
//        self.quizUnlockButton.hidden = YES;
//    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    GWProfile *profile = [GWProfile currentUser];
    
    NSString* game = [NSUserDefaults standardUserDefaults].devicePuzzleType;
    
    if ([game isEqualToString:kPuzzleTypeQuiz]) {
        profile.quizUnlocked = @YES;
    } else if ([game isEqualToString:kPuzzleTypeSpyhole]) {
        profile.spyholeUnlocked = @YES;
    } else if ([game isEqualToString:kPuzzleTypeTiles]) {
        profile.tilesUnlocked = @YES;
    }
    
    if (profile.quizUnlocked.boolValue) {
        self.quizButton.enabled = YES;
        self.quizUnlockButton.hidden = YES;
    } else {
        self.spyholeButton.enabled = NO;
        self.spyholeUnlockButton.hidden = NO;
    }
    
    if (profile.spyholeUnlocked.boolValue) {
        self.spyholeButton.enabled = YES;
                self.spyholeUnlockButton.hidden = YES;
    } else {
        self.spyholeButton.enabled = NO;
        self.spyholeUnlockButton.hidden = NO;
    }
    
    if (profile.tilesUnlocked.boolValue) {
        self.tilesButton.enabled = YES;
        self.tilesUnlockButton.hidden = YES;
    } else {
        self.tilesButton.enabled = NO;
        self.tilesUnlockButton.hidden = NO;
    }
    
    {
        CGRect frame = self.quizButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.quizButton.frame = frame;
    }
    
    {
        CGRect frame = self.spyholeButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.spyholeButton.frame = frame;
    }

    {
        CGRect frame = self.tilesButton.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
        self.tilesButton.frame = frame;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
}

- (IBAction)tilesSelected:(id)sender {
//    GWTilesPuzzleSendViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWTilesPuzzleSendViewController"];
//    vc.image = self.image;
//    vc.whatIs = self.whatIs;
//    vc.profile = self.profile;
//    [self.navigationController pushViewController:vc animated:YES];
    
    GWPuzzle *puzzle = [GWPuzzle MR_createEntity];
    puzzle.type = kPuzzleTypeTiles;
    puzzle.answer = self.whatIs;
    puzzle.order = [[self randomizeOrder] toString];
    puzzle.opponent = self.profile;
    [self sendPuzzle:puzzle];
}

- (NSMutableArray *) randomizeOrder
{
    NSMutableArray *order = [@[@0, @1, @2,
               @3, @4, @5,
               @6, @7, @8, ]mutableCopy];
    static BOOL seeded = NO;
    if(!seeded)
    {
        seeded = YES;
        srandom(time(NULL));
    }
    
    for (int i=0; i<order.count; i++) {
        int nElements = order.count - i;
        int n = (random() % nElements) + i;
        [order exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    return order;
}

- (IBAction)spyholeSelected:(id)sender {
//    GWPuzzleSendViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleSendViewController"];
//    vc.image = self.image;
//    vc.whatIs = self.whatIs;
//    vc.type = kPuzzleTypeSpyhole;
//    vc.profile = self.profile;
//    [self.navigationController pushViewController:vc animated:YES];
    
    GWPuzzle *puzzle = [GWPuzzle MR_createEntity];
    puzzle.type = kPuzzleTypeSpyhole;
    puzzle.answer = self.whatIs;
    puzzle.opponent = self.profile;
    [self sendPuzzle:puzzle];
}
- (IBAction)quizSelected:(id)sender {
//    GWPuzzleSendViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleSendViewController"];
//    vc.image = self.image;
//    vc.whatIs = self.whatIs;
//    vc.type = kPuzzleTypeQuiz;
//    vc.profile = self.profile;
//    [self.navigationController pushViewController:vc animated:YES];
    
    GWPuzzle *puzzle = [GWPuzzle MR_createEntity];
    puzzle.type = kPuzzleTypeQuiz;
    puzzle.answer = self.whatIs;
    puzzle.opponent = self.profile;
    [self sendPuzzle:puzzle];
}

- (void)sendPuzzle:(GWPuzzle*)puzzle {
    
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Sending...";
    [[GWDataLoader sharedInstance] uploadPuzzle:puzzle withImage:self.image withProgressBlock:^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        hud.progress = totalBytesWritten/totalBytesExpectedToWrite;
        
    } andCompletinoBlock:^(id error, id result) {
        [hud hide:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [puzzle MR_deleteEntity];
            GWPuzzleSentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleSentViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }];
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)spyholeUnlockPressed:(id)sender {
    
    [sender setEnabled:NO];
    [[GWDataLoader sharedInstance] buyGameType:kPuzzleTypeSpyhole withCompletionBlock:^(id error, id result) {
        if (!error) {
            self.spyholeButton.enabled = YES;
            [sender setHidden:YES];
            
            CGRect frame = self.spyholeButton.frame;
            frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
            self.spyholeButton.frame = frame;
        } else {
            [GWDataLoader alertViewForError:error];
            [sender setEnabled:YES];
        }
    }];
    
}
- (IBAction)quizUnlockPressed:(id)sender {
    [sender setEnabled:NO];
    [[GWDataLoader sharedInstance] buyGameType:kPuzzleTypeQuiz withCompletionBlock:^(id error, id result) {
        if (!error) {
            self.quizButton.enabled = YES;
            [sender setHidden:YES];
            CGRect frame = self.quizButton.frame;
            frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
            self.quizButton.frame = frame;
        } else {
            [GWDataLoader alertViewForError:error];
            [sender setEnabled:YES];
        }
    }];
}
- (IBAction)tilesUnlockPressed:(id)sender {
    [sender setEnabled:NO];
    [[GWDataLoader sharedInstance] buyGameType:kPuzzleTypeTiles withCompletionBlock:^(id error, id result) {
        if (!error) {
            self.tilesButton.enabled = YES;
            [sender setHidden:YES];
            CGRect frame = self.tilesButton.frame;
            frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
            self.tilesButton.frame = frame;
        } else {
            [GWDataLoader alertViewForError:error];
            [sender setEnabled:YES];
        }
    }];
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
