//
//  GWQuestion.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWQuestion.h"
#import "GWPuzzle.h"


@implementation GWQuestion

@dynamic id;
@dynamic question;
@dynamic answers;
@dynamic correctAnswer;
@dynamic puzzle;

@end
