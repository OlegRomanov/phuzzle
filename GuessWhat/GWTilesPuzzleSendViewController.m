//
//  GWTilesPuzzleSendViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWTilesPuzzleSendViewController.h"
#import "GWPuzzleSentViewController.h"

#import "UICollectionView+Draggable.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "GWTilesCell.h"

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

@interface GWTilesPuzzleSendViewController ()
<UICollectionViewDataSource_Draggable, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    CGFloat _ration;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) NSMutableArray *order;
@end

@implementation GWTilesPuzzleSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.images = [[self.image splitImage] mutableCopy];
    self.order = [@[@0, @1, @2,
                    @3, @4, @5,
                    @6, @7, @8, ]mutableCopy];
    [self randomizeOrder];
    UIImage *image = self.images[0];
    _ration = image.size.height/ image.size.width;
    
    self.collectionView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0.302 alpha:1.0].CGColor;
    self.collectionView.layer.borderWidth = 2.0;
    self.collectionView.layer.cornerRadius = 15.0;
    
    [self applyOrder];
}

- (void) randomizeOrder
{
    static BOOL seeded = NO;
    if(!seeded)
    {
        seeded = YES;
        srandom(time(NULL));
    }
    
    for (int i=0; i<self.order.count; i++) {
        int nElements = self.order.count - i;
        int n = (random() % nElements) + i;
        [self.order exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

- (void) applyOrder
{
    NSMutableArray *tempImages = [NSMutableArray arrayWithCapacity:9];
    for (NSNumber *index in self.order) {
        if (index.integerValue <9) {
            UIImage *image = self.images[index.integerValue];
            [tempImages addObject:image];
        }
    }
    self.images = tempImages;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendPressed:(id)sender {
    GWPuzzle *puzzle = [GWPuzzle MR_createEntity];
    puzzle.type = kPuzzleTypeTiles;
    puzzle.answer = self.whatIs;
    puzzle.order = [self.order toString];
    puzzle.opponent = self.profile;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Sending...";
    [[GWDataLoader sharedInstance] uploadPuzzle:puzzle withImage:self.image withProgressBlock:^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        hud.progress = totalBytesWritten/totalBytesExpectedToWrite;
        
    } andCompletinoBlock:^(id error, id result) {
        [hud hide:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [puzzle MR_deleteEntity];
            GWPuzzleSentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleSentViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GWTilesCell *cell = (GWTilesCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"GWTilesCell" forIndexPath:indexPath];

    cell.imageView.image = self.images[indexPath.row];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(99, 99);
    
    size.height *= _ration;
    
    return size;
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSString *index = [self.images objectAtIndex:fromIndexPath.item];
    NSString *index2 = [self.images objectAtIndex:toIndexPath.item];
    [self.images removeObjectAtIndex:fromIndexPath.item];
    [self.images insertObject:index atIndex:toIndexPath.item];
    
    [self.images removeObject:index2];
    [self.images insertObject:index2 atIndex:fromIndexPath.item];
    
    NSString *oindex = [self.order objectAtIndex:fromIndexPath.item];
    NSString *oindex2 = [self.order objectAtIndex:toIndexPath.item];
    [self.order removeObjectAtIndex:fromIndexPath.item];
    [self.order insertObject:oindex atIndex:toIndexPath.item];
    
    [self.order removeObject:oindex2];
    [self.order insertObject:oindex2 atIndex:fromIndexPath.item];
}


@end
