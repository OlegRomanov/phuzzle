//
//  GWGameTimerView.h
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWGameTimerView;

@protocol GWGameTimerViewDelegate <NSObject>

- (void) gameTimerViewDidEnded:(GWGameTimerView*) view;
- (void) gameTimerViewDidBakcPressed:(GWGameTimerView*) view;

- (BOOL) gameTimerViewGameActive:(GWGameTimerView *)view;

@end

@interface GWGameTimerView : UIView

+ (GWGameTimerView*) viewWithDelegate:(id<GWGameTimerViewDelegate>) delegate;

@property (strong, nonatomic) IBOutlet UILabel *timerLabel;

@property (nonatomic, strong) id<GWGameTimerViewDelegate> delegate;
@property (nonatomic, readonly) BOOL isPaused;
@property (nonatomic, readonly) BOOL isStarted;

-(void) startTimer:(NSTimeInterval)seconds;
-(NSInteger) stopTimer;

- (void) pauseTimer;
- (void) continueTimer;

@end
