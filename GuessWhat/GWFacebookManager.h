//
//  GWFacebookManager.h
//  Phuzzle
//
//  Created by Artem Antipov on 23.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kFBUsernameKey;
extern NSString* const kFBNotificationLoggedIn;
extern NSString* const kFBNotificationLoggedCancel;
extern NSString* const kFBNotificationLoggedOut;

extern NSString* const kFBNotificationSharedToWall;
extern NSString* const kFBNotificationPostedToWall;

extern NSString* const kFBNotificationSharedToWallFailed;
extern NSString* const kFBNotificationPostedToWallFailed;

typedef void(^GWFBCompletionBlock)(id error, BOOL success);

@interface GWFacebookManager : NSObject

@property (nonatomic, strong) NSString *postToWallText;
@property (readwrite, copy) GWComletionBlock postToWallBlock;

@property (nonatomic, strong) NSString *shareToWallText;
@property (nonatomic, strong) UIImage *shareToWallImage;
@property (readwrite, copy) GWComletionBlock shareToWallBlock;

+ (GWFacebookManager*)sharedInstance;

- (void) lounch;
- (void) handleDidBecomeActive;
- (BOOL) openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

- (void) loginWithCompletionBlock:(GWFBCompletionBlock) block;
- (void) logout;

- (BOOL) isSessionOpen;
- (BOOL) isSessionExtended;

- (void) fetchFriendsWithCompletionBlock:(GWComletionBlock) block;
- (void) fetchSelfWithCompletionBlock:(GWComletionBlock) block;

- (void) postToWall:(NSString*) text withCompletionBlock:(GWComletionBlock) block;

- (void) sharedToWall:(NSString*) text withImage:(UIImage*)image withCompletionBlock:(GWComletionBlock) block;

+ (NSString *) errorMessage:(NSError*) fbError;

@end
