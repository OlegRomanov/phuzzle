//
//  GWCameraViewController.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"

@class GWCameraViewController;
@class GWProfile;

@protocol GWCameraViewControllerDelegate <NSObject>

- (void) didCapturedImage:(UIImage*) image withName:(NSString*) text toProfile:(GWProfile*)profile;

- (void) captureImageWillDissmis;
- (void) captureImageDidDissmis;

@end

@interface GWCameraViewController : GWBaseViewController

@property (nonatomic, strong) GWProfile *profile;

@property (nonatomic, weak) id<GWCameraViewControllerDelegate> delegate;

@end
