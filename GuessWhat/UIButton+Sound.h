//
//  UIButton+Sound.h
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Sound)

@property (nonatomic, readwrite) BOOL isMute;
@property (nonatomic, strong) NSString *sound;

@end
