//
//  GWTitleBar.h
//  Phuzzle
//
//  Created by Artem Antipov on 20.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWTitleBar : UIView

@property (nonatomic, readwrite) UIImage * bgImage;

+ (GWTitleBar*) view;

@end
