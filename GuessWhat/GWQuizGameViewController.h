//
//  GWQuizGameViewController.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBaseViewController.h"
#import "GWBaseGameViewController.h"


@interface GWQuizGameViewController : GWBaseGameViewController

@end
