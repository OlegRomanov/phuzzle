//
//  GWGameTimerView.m
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWGameTimerView.h"

@interface GWGameTimerView ()
{
    NSInteger _seconds;
    NSTimer *_timer;
    BOOL _isPaused;
    BOOL _isStarted;
    GWAudioPlayer *_clockSound;
    
    NSTimeInterval  countSeconds;
}

@end

@implementation GWGameTimerView

+ (GWGameTimerView*) viewWithDelegate:(id<GWGameTimerViewDelegate>) delegate
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWGameTimerView" owner:self options:nil];
    
    GWGameTimerView *view = (GWGameTimerView*)[nib objectAtIndex:0];
    view.delegate = delegate;
    
    
    return  view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)dealloc
{
    [self stopTimer];
}

-(void) startTimer:(NSTimeInterval)seconds
{
    dispatch_async(dispatch_get_main_queue(), ^{
        countSeconds = seconds;
        _clockSound = [[GWAudioPlayer alloc] initWithSound:@"Clock"];
        [_clockSound playIfSoundisEnabled];
        [NSUserDefaults standardUserDefaults].startTimerTime = [NSDate date];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(pauseTimer)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(continueTimer)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        _isStarted = YES;
        _seconds = seconds;
        self.timerLabel.text = [NSString stringWithFormat:@"%@", @(_seconds)];
        if (_timer) {
            [self stopTimer];
        }
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(reduceSecond) userInfo:@{} repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];

    });
}
- (IBAction)backPressed:(id)sender
{
    if (self.delegate) {
        [self.delegate gameTimerViewDidBakcPressed:self];
    }
}

- (NSInteger) stopTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [_timer invalidate];
        _timer = nil;
        
        [_clockSound stopSound];
    });
    return _seconds;
}

-(void) reduceSecond
{
    _seconds--;
    self.timerLabel.text = [NSString stringWithFormat:@"%@", @(_seconds)];
    if (_seconds==10 || _seconds == 20|| _seconds == 30|| _seconds == 40) {
        [_clockSound stopSound];
        _clockSound = [[GWAudioPlayer alloc] initWithSound:@"Clock"];
        [_clockSound playIfSoundisEnabled];
    }
    if (_seconds <= 0) {
        if (self.delegate && [self.delegate gameTimerViewGameActive:self]) {
            [self.delegate gameTimerViewDidEnded:self];
        }
        [self stopTimer];
    }

}

- (void) pauseTimer
{
    _isPaused = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [_timer invalidate];
        _timer = nil;
    });
}

- (void) continueTimer
{
    _isPaused = NO;
    NSTimeInterval time = (-1) * [[NSUserDefaults standardUserDefaults].startTimerTime timeIntervalSinceNow];
    if (time < countSeconds) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _seconds = countSeconds - time;
            self.timerLabel.text = [NSString stringWithFormat:@"%@", @(_seconds)];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(reduceSecond) userInfo:@{} repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        });
    } else {
        if (self.delegate) {
            [self.delegate gameTimerViewDidEnded:self];
        }
    }
}



@end
