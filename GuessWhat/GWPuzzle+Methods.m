//
//  GWPuzzle+Methods.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"
#import "GWQuestion+Methods.h"

NSString* const kPuzzleTypeTiles = @"tiles";
NSString* const kPuzzleTypeSpyhole = @"spy hole";
NSString* const kPuzzleTypeQuiz = @"query";

@implementation GWPuzzle (Methods)

- (NSDictionary*) toDictionary
{
    NSMutableDictionary *jsonDict = [@{
                               @"type_puzzle":self.type,
                               @"correct_answer":self.answer,
                               @"player_to":self.opponent.id,
                               } mutableCopy];
    
    if (self.order) {
        [jsonDict setObject:self.order forKey:@"tiles_orders"];
    }
    
    return jsonDict;
}

+ (GWPuzzle*) createEntryForID:(NSNumber*) ID
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %@", ID];
    GWPuzzle *puzzle = [GWPuzzle MR_findFirstWithPredicate:pred];
    if (!puzzle) {
        puzzle = [GWPuzzle MR_createEntity];
        puzzle.id = ID;
    }
    return puzzle;
}

+ (GWPuzzle*) createWithDict:(NSDictionary*) dict
{
    if (!dict) {
        return nil;
    }
    if (!dict[@"id"]) {
        return nil;
    }
    GWPuzzle * puzzle = [GWPuzzle createEntryForID:dict[@"id"]];
    
    puzzle.answer = dict[@"correct_answer"] ? dict[@"correct_answer"] : @"";
    puzzle.imageURL = dict[@"url_image"] ? dict[@"url_image"] : @"";
    puzzle.order = dict[@"tiles_orders"] ? dict[@"tiles_orders"] : @"";
    puzzle.type = dict[@"type_puzzle"] ? dict[@"type_puzzle"] : @"";
    
    GWProfile *from = [GWProfile createWithDict:dict[@"player_from"]];
    GWProfile *to = [GWProfile createWithDict:dict[@"player_to"]];
    
    if ([from.id isEqualToNumber:[GWProfile currentUser].id]) {
        puzzle.opponent = to;
        puzzle.my = @YES;
    } else {
        puzzle.opponent = from;
        puzzle.my = @NO;
    }
    
    if (puzzle.questions.allObjects.count > 0) {
        [puzzle removeQuestions:puzzle.questions];
    }
    
    if (dict[@"questions"] && [dict[@"questions"]count]>0) {
        for (NSDictionary *quest in dict[@"questions"]) {
            GWQuestion *question = [GWQuestion createEntryForID:quest[@"id"]];
            
            question.question = quest[@"text"];
            question.answers = quest[@"answers"];
            question.correctAnswer = quest[@"correct_answer"];
            
            [puzzle addQuestionsObject:question];
        }
    }
    if ([puzzle.type isEqualToString:kPuzzleTypeQuiz]) {
        if (puzzle.questions.allObjects.count <= 0) {
            NSLog(@"QUIZ %@", puzzle);
            NSLog(@"No QUESTIONS");
        } else {
            NSLog(@"QUIZ %@", puzzle);
            NSLog(@"QUIZ HAVE #%@ QUESTIONS", @(puzzle.questions.allObjects.count));
        }
    }
    
    return puzzle;
}

@end
