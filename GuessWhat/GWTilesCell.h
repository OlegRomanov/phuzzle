//
//  GWTilesCell.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWTilesCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
