//
//  GWStoreHelper.h
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "IAPHelper.h"

@interface GWStoreHelper : IAPHelper

+ (GWStoreHelper *)sharedInstance;

@end
