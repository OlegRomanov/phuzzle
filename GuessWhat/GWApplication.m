//
//  GWApplication.m
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWApplication.h"

@implementation GWApplication


- (BOOL)sendAction:(SEL)action to:(id)target from:(id)sender forEvent:(UIEvent *)event{
    
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *button = sender;
        if (!button.isMute) {
            if (button.sound) {
                [[[GWAudioPlayer alloc] initWithSound:button.sound ] playIfSoundisEnabled];
            } else {
                [[GWAudioPlayer playerWithDefaulClick] playIfSoundisEnabled];
            }
        }
        
    }
    return [super sendAction:action to:target from:sender forEvent:event];
}

@end
