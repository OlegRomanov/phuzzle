//
//  GWBlurFilterMask.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWBlurFilterMask.h"

CGFloat const GRADIENT_WIDTH = 50.0f;

@implementation GWBlurFilterMask

- (void)drawInContext:(CGContextRef)context
{
    CGFloat clearRegionRadius = self.diameter * 0.5f;
    CGFloat blurRegionRadius = clearRegionRadius + GRADIENT_WIDTH;
    
    CGColorSpaceRef baseColorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat colours[8] = { 0.0f, 0.0f, 0.0f, 0.0f,     // Clear region colour.
        0.0f, 0.0f, 0.0f, 1.0f };   // Blur region colour.
    CGFloat colourLocations[2] = { 0.0f, 0.4f };
    CGGradientRef gradient = CGGradientCreateWithColorComponents (baseColorSpace, colours, colourLocations, 2);
    
    CGContextDrawRadialGradient(context, gradient, self.origin, clearRegionRadius, self.origin, blurRegionRadius, kCGGradientDrawsAfterEndLocation);
    
    CGColorSpaceRelease(baseColorSpace);
    CGGradientRelease(gradient);
}

@end
