//
//  GWButton.m
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "GWButton.h"

@implementation GWButton
+(id)buttonWithType:(UIButtonType)buttonType
{
    UIButton *button = [super buttonWithType:buttonType];
    [button addTarget:button action:@selector(buttonPressed) forControlEvents:UIControlEventTouchDown];
    return button;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchDown];
    }
    return self;
}

-(void) buttonPressed
{
    NSLog(@"GWButton pressed");
//    [[GWAudioPlayer playerWithDefaulClick] playIfSoundisEnabled];
}

@end
