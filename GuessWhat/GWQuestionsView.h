//
//  GWQuestionsView.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWQuestionsView;
@class GWQuestion;

@protocol GWQuestionsViewDelegate <NSObject>

- (void) questionViewDidPressedRightAnswer:(GWQuestionsView*) view;
- (void) questionViewDidPressedWrongAnswer:(GWQuestionsView*) view;

@end

@interface GWQuestionsView : UIView

@property (nonatomic, weak) id<GWQuestionsViewDelegate> delegate;

@property (nonatomic, strong) GWQuestion *question;

+ (GWQuestionsView*) viewWithDelegate:(id<GWQuestionsViewDelegate>) delegate andQuestion:(GWQuestion*) question;

@end
