//
//  GWGamesViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <AdColony/AdColony.h>
#import "GWGamesViewController.h"
#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"

#import "GWTilesGameViewController.h"
#import "GWSpyholeGameViewController.h"
#import "GWQuizGameViewController.h"


@interface GWGamesViewController ()
        <GWCameraViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, AdColonyAdDelegate> {
    BOOL adNeedPlay;
}
@property(nonatomic, strong) GWPuzzle *phuzzle;
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableSet *playersSet;
@property(nonatomic, strong) NSMutableArray *players;

@end

#define kShowAlert @"Show alert"

@implementation GWGamesViewController

- (void)awakeFromNib {
    [super awakeFromNib];

    self.tittleBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    __weak typeof (self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf loadGames];
    }                                        position:SVPullToRefreshPositionTop];

    self.tableView.pullToRefreshView.arrowColor = [UIColor defaultTextColor];
    self.tableView.pullToRefreshView.textColor = [UIColor defaultTextColor];
    self.tableView.pullToRefreshView.activityIndicatorViewColor = [UIColor defaultTextColor];

}


- (void)viewWillAppear:(BOOL)animated {
    if (adNeedPlay) {
        return;
    }
    bool wasAlertShown = [[NSUserDefaults standardUserDefaults] boolForKey:kShowAlert];

    if (!wasAlertShown) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Invite Friends to Start Playing!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"DON'T SHOW AGAIN", nil];
        alert.delegate = self;
        [alert show];
    }

    [super viewWillAppear:animated];

    [self.tableView triggerPullToRefresh];
    [self loadGames];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadGames {
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSUserDefaults standardUserDefaults].recivedPuzzleID = @0;
    [[GWDataLoader sharedInstance] loadAllGamesWithCompletionBlock:^(id error, id result) {
        //        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.tableView.pullToRefreshView stopAnimating];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
            self.players = result;
            [self.tableView reloadData];
        }
    }];
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)createPuzzlePressed:(id)sender {
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"NavControllerGWCameraViewController"];
    GWCameraViewController *vc = (GWCameraViewController *) nc.topViewController;
    vc.delegate = self;
    if ([sender isKindOfClass:[NSManagedObject class]]) {
        vc.profile = sender;
    }
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)didCapturedImage:(UIImage *)image withName:(NSString *)text toProfile:(GWProfile *)profile {
    GWPuzzleTypeSelectViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWPuzzleTypeSelectViewController"];
    vc.image = image;
    vc.whatIs = text;
    vc.profile = profile;

    [self.navigationController pushViewController:vc animated:YES];
}

- (void)captureImageDidDissmis {
}

- (void)captureImageWillDissmis {
}

#pragma mark - tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.players.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell"];
    GWProfile *prfile = [self.players objectAtIndex:indexPath.row];

    UILabel *nameLabel = (UILabel *) [cell viewWithTag:102];
    nameLabel.text = prfile.username;

    UILabel *winsLabel = (UILabel *) [cell viewWithTag:103];
    winsLabel.text = [NSString stringWithFormat:@"%@", prfile.higestConsecutiveWins];

    UIImageView *gameLabel = (UIImageView *) [cell viewWithTag:101];
    [[gameLabel.superview viewWithTag:111] removeFromSuperview];
    GWPuzzle *puzzle = prfile.puzzle;
    if (puzzle) {
        if (puzzle.my.boolValue) {
            UILabel *label = [[UILabel alloc] initWithFrame:gameLabel.frame];
            label.text = @"WAITING...";
            label.font = [UIFont defaultFontWithSize:18];
            [label sizeToFit];
            label.center = gameLabel.center;
            label.textColor = [UIColor whiteColor];
            label.tag = 111;
            label.textAlignment = NSTextAlignmentCenter;
            [gameLabel.superview addSubview:label];
            gameLabel.image = [UIImage imageNamed:@""];
        } else {
            gameLabel.image = [UIImage imageNamed:@"SOLVE BUTTON"];
        }
    } else {
        gameLabel.image = [UIImage imageNamed:@"PLAY BUTTON"];
    }


    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [[[GWAudioPlayer alloc] initWithSound:@"ButtonPop"] playIfSoundisEnabled];
    GWProfile *prfile = [self.players objectAtIndex:indexPath.row];
    if (prfile.puzzle) {
        GWPuzzle *puzzle = prfile.puzzle;
        if (puzzle.my.boolValue) {
            //            gameLabel.text = @"Waiting";
        } else {
            self.phuzzle = puzzle;
            //TODO COLONY replace with YOUR ZONE ID
            [AdColony playVideoAdForZone:@"vzfc98660c1bcf48fa93" withDelegate:self];
        }
    } else {
        //        gameLabel.text = @"Play";
        [self createPuzzlePressed:prfile];
    }
}

- (void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here to do what you want when you hit del
        GWProfile *prfile = [self.players objectAtIndex:indexPath.row];
        NSString *gameID = [prfile.id stringValue];
        [[GWDataLoader sharedInstance] deleteGame:prfile.gameIdentify withCompletionBlock:^(id error, id result) {
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
            if (error) {
                [GWDataLoader alertViewForError:error];
            } else {
                if (result && [result isKindOfClass:[NSNumber class]]) {
                    bool success = [result boolValue];
                    if (success) {
                        [self.players removeObjectAtIndex:[indexPath row]];
                        [tableView reloadData];
                    }
                }
            }
        }];
        }
    }

#pragma mark - AdColonyAdDelegate

// Is called when AdColony has taken control of the device screen and is about to begin showing an ad
// Apps should implement app-specific code such as pausing a game and turning off app music
- (void)onAdColonyAdStartedInZone:(NSString *)zoneID {
    adNeedPlay = YES;
}

// Is called when AdColony has finished trying to show an ad, either successfully or unsuccessfully
// If shown == YES, an ad was displayed and apps should implement app-specific code such as unpausing a game and restarting app music
- (void)onAdColonyAdAttemptFinished:(BOOL)shown inZone:(NSString *)zoneID {
    adNeedPlay = NO;
    if (shown) {
        [self playPuzzle];
    } else {
        NSLog(@"AdColony did not play an ad for zone %@", zoneID);
        [self playPuzzle];
    }
}

- (void)playPuzzle {
    if (!self.phuzzle) {
        self.phuzzle = self.recivedPuzzle;
    }
    if ([self.phuzzle.type isEqualToString:kPuzzleTypeTiles]) {
        GWTilesGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWTilesGameViewController"];
        vc.puzzle = self.phuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([self.phuzzle.type isEqualToString:kPuzzleTypeSpyhole]) {
        GWSpyholeGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWSpyholeGameViewController"];
        vc.puzzle = self.phuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([self.phuzzle.type isEqualToString:kPuzzleTypeQuiz]) {
        GWQuizGameViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWQuizGameViewController"];
        vc.puzzle = self.phuzzle;
        [self.navigationController pushViewController:vc animated:YES];
    }

    {
        self.phuzzle = nil;
        self.recivedPuzzle = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShowAlert];
    }
}

@end
