//
//  GWBaseViewController.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWPuzzle;

@interface GWBaseViewController : UIViewController
<AdColonyAdDelegate, UIAlertViewDelegate>

@property (nonatomic,readwrite) BOOL tittleBarHidden;
@property (nonatomic,readwrite) BOOL noADS;
@property (nonatomic,readwrite) BOOL notRecivePushes;

@property (nonatomic, retain) GWPuzzle *recivedPuzzle;

@property (nonatomic,strong) NSString* bgImageName;

- (void)playPuzzle:(GWPuzzle *)repeatPuzzle;
@end
