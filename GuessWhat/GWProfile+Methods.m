//
//  GWProfile+Methods.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWProfile+Methods.h"
#import "GWPuzzle+Methods.h"

@implementation GWProfile (Methods)

+ (GWProfile*) currentUser
{
    NSNumber *idNbr = [[NSUserDefaults standardUserDefaults] currentUserID];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %@", idNbr];
    GWProfile *user = [GWProfile MR_findFirstWithPredicate:pred];
    return user;
}

+ (GWProfile*) createEntryForID:(NSNumber*) ID
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %@", ID];
    GWProfile *profile = [GWProfile MR_findFirstWithPredicate:pred];
    if (!profile) {
        profile = [GWProfile MR_createEntity];
        profile.id = ID;
    }
    return profile;
}

+ (GWProfile*) createWithDict:(NSDictionary*) dict
{
    GWProfile * profile = [GWProfile createEntryForID:dict[@"id"]];
    
    profile.email = dict[@"email"] ? dict[@"email"] : @"";
    profile.username = dict[@"username"] ? dict[@"username"] : @"";
    profile.facebookID = dict[@"facebook_id"] ? dict[@"facebook_id"] : @0;
    
    profile.adClicks = dict[@"ad_clicks"] ? dict[@"ad_clicks"] : @0;
    profile.referrals = dict[@"referrals"] ? dict[@"referrals"] : @0;
    
    profile.higestConsecutiveWins = dict[@"higest_consecutive_wins"] ? dict[@"higest_consecutive_wins"] : @0;
    profile.keys = dict[@"number_keys"] ? dict[@"number_keys"] : @0;
    profile.coins = dict[@"coins"] ? dict[@"coins"] : @0;
    
    if ([dict[@"id"] longLongValue] == [[self currentUser].id longLongValue]) {
        [self reportScore:profile.higestConsecutiveWins.longLongValue forLeaderboardID:@"grp.com.Phuzzle.hcwLeaderboard"];
        [self reportScore:profile.keys.longLongValue forLeaderboardID:@"grp.com.Phuzzle.keysLeaderboard"];
        [self reportScore:profile.coins.longLongValue forLeaderboardID:@"grp.com.Phuzzle.coninsLeaderboard"];
        [[GWGameCenterHelper sharedInstance] reportAchievementIdentifier:GWAchiviment100Keys percentComplete:profile.keys.longLongValue];
    }
    
    
    NSString *unlokedString = dict[@"unlocked_types_games"] ? dict[@"unlocked_types_games"] : nil;
    if (unlokedString && unlokedString.length>0) {
        NSArray *components = [unlokedString componentsSeparatedByString:@","];
        for (NSString* game in components) {
            if ([game isEqualToString:kPuzzleTypeQuiz]) {
                profile.quizUnlocked = @YES;
            } else if ([game isEqualToString:kPuzzleTypeSpyhole]) {
                profile.spyholeUnlocked = @YES;
            } else if ([game isEqualToString:kPuzzleTypeTiles]) {
                profile.tilesUnlocked = @YES;
            }
        }
    }
    
    return profile;
}

+ (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) identifier
{
    NSString *reqSysVer = @"7.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    GKScore *scoreReporter;
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier: identifier];
    } else {
        scoreReporter = [[GKScore alloc] initWithCategory:identifier];
    }
    scoreReporter.value = score;
    scoreReporter.context = 0;
    
    NSArray *scores = @[scoreReporter];
    [GKScore reportScores:scores withCompletionHandler:^(NSError *error) {
    }];
}

@end
