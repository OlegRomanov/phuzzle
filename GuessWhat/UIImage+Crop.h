//
//  UIImage+Crop.h
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Crop)

- (UIImage *) imageCroppedWithRect:(CGRect)rect;
- (NSArray*) splitImage;

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

@end
