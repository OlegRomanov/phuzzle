//
//  GWAppDelegate.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWAppDelegate.h"

@interface GWAppDelegate ()
{
    BOOL _willEnterForeground;
}

@end

@implementation GWAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Override point for customization after application launch.
    
    [RevMobAds startSessionWithAppID:@"52bc88005c4f67e593000025"];
   
    
    //TODO AD COLONY your ID and ZONEID
    [AdColony configureWithAppID:@"appe65f5b97667746d7ae" zoneIDs:@[@"vz4f2b5996b41c47a8b0", @"vzfc98660c1bcf48fa93"] delegate:nil logging:YES];
    
    [GWDataLoader sharedInstance];
    
    [MagicalRecord setupAutoMigratingCoreDataStack];
    [GWStoreHelper sharedInstance];
    [[GWFacebookManager sharedInstance] lounch];
    
    if (launchOptions != nil) {
        // Launched from push notification
        NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        NSLog(@"Launched with notification: %@", notification);
        [self didReciveRemoteNotification:notification];
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // 1
    _willEnterForeground = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // 3
    [[GWFacebookManager sharedInstance] handleDidBecomeActive];
    _willEnterForeground = NO;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [[GWFacebookManager sharedInstance] openURL:url sourceApplication:sourceApplication];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken  {
    NSString *token = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"My token is: %@", token);
    [NSUserDefaults standardUserDefaults].deviceToken = token;
    if ([NSUserDefaults standardUserDefaults].sessionToken) {
        [[GWDataLoader sharedInstance] registerDeviceToken];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to get token, error: %@", error);
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Received remote notification: %@", userInfo);
    
    [self didReciveRemoteNotification:userInfo];
}

- (void) didReciveRemoteNotification:(NSDictionary*) userInfo
{
    // 2
    [NSUserDefaults standardUserDefaults].shouldStartGame = _willEnterForeground;
    [NSUserDefaults standardUserDefaults].recivedPuzzleID = userInfo[@"puzzle_id"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRecivePuzzle object:nil];
}




@end
