//
//  GWQuestionsView.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWQuestionsView.h"

#import "GWQuestion+Methods.h"

@interface GWQuestionsView ()
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end


@implementation GWQuestionsView

+ (GWQuestionsView*) viewWithDelegate:(id<GWQuestionsViewDelegate>) delegate andQuestion:(GWQuestion*) question
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GWQuestionsView" owner:self options:nil];
    
    GWQuestionsView *view = (GWQuestionsView*)[nib objectAtIndex:0];
    view.backgroundColor = [UIColor clearColor];
    view.delegate = delegate;
    view.question = question;
    
    return  view;
}

-(void)setQuestion:(GWQuestion *)question
{
    _question = question;
    
    self.questionLabel.text = question.question;
    NSString *reqSysVer = @"7.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
    {
        self.questionLabel.font = [UIFont defaultFontWithSize:self.questionLabel.font.pointSize];
        self.questionLabel.minimumScaleFactor = 0.4;
        self.questionLabel.adjustsFontSizeToFitWidth = YES;
    } else {
        self.questionLabel.font = [UIFont defaultFontWithSize:18];
        self.questionLabel.adjustsFontSizeToFitWidth = NO;
    }
    
//    self.questionLabel.numberOfLines=0;
//    self.questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSArray *answers = [NSArray arrayStringsWithString:question.answers];
    
    CGFloat lenght = 0;
    
    for (NSString *answer in answers) {
        UIButton *answerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [answerButton setTitle:answer forState:UIControlStateNormal];
        answerButton.titleLabel.font = [UIFont defaultFontWithSize:answerButton.titleLabel.font.pointSize];
        [answerButton sizeToFit];
        [answerButton addTarget:self action:@selector(answerPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect frame = answerButton.frame;
        frame.origin.x = lenght + 20;
        frame.origin.y = 0;
        frame.size.height = 50;
        
        answerButton.frame = frame;
        answerButton.isMute = YES;
        [self.scrollView addSubview:answerButton];
        
        lenght += 20 + frame.size.width;
    }
    
    self.scrollView.contentSize = CGSizeMake(lenght+20, 50);
    if (self.scrollView.frame.size.width > lenght+20){
        CGRect frame = self.scrollView.frame;
        frame.origin.x = (self.frame.size.width - lenght+20)/2;
        self.scrollView.frame = frame;
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if (self.scrollView.frame.size.width > self.scrollView.contentSize.width){
        CGRect frame = self.scrollView.frame;
        frame.origin.x = (self.frame.size.width - self.scrollView.contentSize.width)/2;
        self.scrollView.frame = frame;
    }
}

- (void) answerPressed:(UIButton*) sender
{
    NSString *answer = sender.titleLabel.text;
    
    if ([[answer uppercaseString] isEqualToString:[self.question.correctAnswer uppercaseString]]) {
        [self.delegate questionViewDidPressedRightAnswer:self];
    } else {
        [self.delegate questionViewDidPressedWrongAnswer:self];
    }
}


@end
