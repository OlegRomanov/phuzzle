//
//  GWGameRevealViewViewController.h
//  Phuzzle
//
//  Created by Sunny on 2/24/14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWBaseViewController.h"
@interface GWGameRevealViewViewController : UIViewController
@property (nonatomic, retain) GWPuzzle *puzzle;
@property (weak, nonatomic) IBOutlet UILabel *lblAnswer;
@property (weak, nonatomic) IBOutlet UIImageView *ivCorrect;

@end
