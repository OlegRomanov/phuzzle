//
//  UIButton+Sound.m
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import "UIButton+Sound.h"
#import "GWButtonsSingltone.h"


@implementation UIButton (Sound)


-(void)setSound:(NSString *)sound
{
    [[GWButtonsSingltone sharedInstance].buttonsSound setObject:sound forKey:@(self.hash)];
}

-(NSString *)sound
{
    NSString *sound = [GWButtonsSingltone sharedInstance].buttonsSound[@(self.hash)];
    return  sound;
}

-(void)setIsMute:(BOOL)isMute
{
    [[GWButtonsSingltone sharedInstance].buttonsMute setObject:@(isMute) forKey:@(self.hash)];
}

-(BOOL)isMute
{
    return [[GWButtonsSingltone sharedInstance].buttonsMute[@(self.hash)] boolValue];
}

@end
