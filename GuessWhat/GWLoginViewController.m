//
//  GWLoginViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWLoginViewController.h"
#import "GWProfile.h"
#import "GWMainViewController.h"
#import "GWRegistrationViewController.h"

@interface GWLoginViewController ()
<UITextFieldDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet GWTextField *usernameField;
@property (strong, nonatomic) IBOutlet GWTextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *remeberButton;
@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@end

@implementation GWLoginViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.noADS = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signInButton.sound = @"UserLoginLogout";
    self.fbButton.sound = @"UserLoginLogout";

	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [NSUserDefaults standardUserDefaults].rememberSession = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadFBProfile)
                                                 name:kFBNotificationLoggedIn
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kFBNotificationLoggedIn
                                                  object:nil];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = self.scrollView.frame.size;
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
//    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    double duration = [number doubleValue];

    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom + size.height, self.scrollView.contentInset.right)];
}

-(void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
//    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    double duration = [number doubleValue];
    [self.scrollView setContentInset:UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, self.scrollView.contentInset.bottom - size.height , self.scrollView.contentInset.right)];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameField) {
        [self.passwordField becomeFirstResponder];
    } else if(textField == self.passwordField){
        [self loginPressed:textField];
    }
    return NO;
}

- (IBAction)remeberPressed:(UIButton*)sender
{
    sender.selected = !sender.selected;
    [NSUserDefaults standardUserDefaults].rememberSession = sender.selected;
}

- (IBAction)loginPressed:(id)sender
{
    if(![self validate]){
        return;
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GWDataLoader sharedInstance] loginWithUsername:self.usernameField.text withPassword:self.passwordField.text andCompletinoBlock:^(id error, id result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
            if (![NSUserDefaults standardUserDefaults].devicePuzzleType) {
                [self performSegueWithIdentifier:@"LoginToPickPuzzle" sender:self];
            }else{
                GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
    }];
}
- (IBAction)registrationPressed:(id)sender
{
    GWRegistrationViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWRegistrationViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)facebookPressed:(id)sender
{
    
    [[GWFacebookManager sharedInstance] loginWithCompletionBlock:^(NSError *error, BOOL success) {
//        if (success) {
////            [self loadFBProfile];
//        } else {
//            if (error) {
////                NSString *reason = error.userInfo[@"com.facebook.sdk:ErrorLoginFailedReason"];
//                NSString *message = [FBErrorUtility userMessageForError:error];
////                if ([reason isEqualToString:@"com.facebook.sdk:SystemLoginDisallowedWithoutError"]) {
////                    message = @"To use your Facebook account with this app, open Settings > Facebook and make sure this app is turned on.";
////                }
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook Error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alertView show];
//            }
//            
//        }
    }];
}

-( void) loadFBProfile
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GWFacebookManager sharedInstance] fetchSelfWithCompletionBlock:^(id error, id result) {
        if (!error) {
            if (result) {
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                NSNumber * ID = [f numberFromString:result[@"id"]];
                [self fbLogin:result[kFBUsernameKey] andID:ID];
            }
            
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }];
}

- (void) fbLogin:(NSString*)username andID:(NSNumber*)fbid
{
    [[GWDataLoader sharedInstance] loginWithFacebook:username withID:fbid andCompletinoBlock:^(id error, id result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [GWDataLoader alertViewForError:error];
        } else {
            if (![NSUserDefaults standardUserDefaults].devicePuzzleType) {
                [self performSegueWithIdentifier:@"LoginToPickPuzzle" sender:self];
            }else{
                GWMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GWMainViewController"];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }];
}
- (BOOL) validate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What The Phuzzle?!" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    if (self.usernameField.text.length == 0) {
        alert.title = @"Please Enter Your Username";
        [alert show];
        return NO;
    }
    if (self.passwordField.text.length == 0) {
        alert.title = @"Please Enter Your Password";
        [alert show];
        return NO;
    }
    
    return YES;
}

@end
