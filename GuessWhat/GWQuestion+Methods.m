//
//  GWQuestion+Methods.m
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWQuestion+Methods.h"

@implementation GWQuestion (Methods)

+ (GWQuestion*) createEntryForID:(NSNumber*) ID
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %@", ID];
    GWQuestion *puzzle = [GWQuestion MR_findFirstWithPredicate:pred];
    if (!puzzle) {
        puzzle = [GWQuestion MR_createEntity];
        puzzle.id = ID;
    }
    return puzzle;
}

@end
