//
//  GWMainNavigationViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 17.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWMainNavigationViewController.h"

#import "GWTitleBar.h"

@interface GWMainNavigationViewController ()

@end

@implementation GWMainNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    self.navigationBarHidden = YES;
    

    self.bar = [GWTitleBar view];
    self.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationBar.tintColor = [UIColor clearColor];
    [self.navigationBar addSubview:self.bar];
    
    [self.navigationItem setHidesBackButton:YES];
    
    [self setDefaultFont:self.view];
}

- (void) setDefaultFont:(UIView*) superview
{
    for (UIView *view  in superview.subviews) {
        
        if ([view isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel*) view;
            label.font = [UIFont defaultFontWithSize:label.font.pointSize];
        } else if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*) view;
            button.titleLabel.font = [UIFont defaultFontWithSize:button.titleLabel.font.pointSize];
        } else if ([view isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*) view;
            textField.font = [UIFont defaultFontWithSize:textField.font.pointSize];
        } else if ([view isKindOfClass:[UITextView class]]) {
            UITextView *textField = (UITextView*) view;
            textField.font = [UIFont defaultFontWithSize:textField.font.pointSize];
        } else {
            [self setDefaultFont:view];
        }
    }
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
	return self.topViewController.supportedInterfaceOrientations;
}


@end
