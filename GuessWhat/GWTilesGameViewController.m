//
//  GWTilesGameViewController.m
//  GuessWhat
//
//  Created by Artem Antipov on 18.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import "GWTilesGameViewController.h"
#import "UICollectionView+Draggable.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "GWTilesCell.h"
#import "GWAnswerView.h"
#import "GWGameTimerView.h"

#import "GWGameAlertView.h"

#import "GWCameraViewController.h"
#import "GWPuzzleTypeSelectViewController.h"

#import "GWPuzzle+Methods.h"
#import "GWProfile+Methods.h"

@interface GWTilesGameViewController ()
<UICollectionViewDataSource_Draggable, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    CGFloat _ratio;
    
}

@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) NSMutableArray *order;

@property (strong, nonatomic) IBOutlet UIView *timerContainerView;
@property (strong, nonatomic) IBOutlet UIView *controllContainerView;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation GWTilesGameViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.puzzle) {
        self.puzzle = [GWPuzzle MR_createEntity];
        self.puzzle.type = kPuzzleTypeTiles;
        self.puzzle.answer = @"Kangaroo";
        self.puzzle.order = @"8,7,6,5,4,3,2,1,0";
        self.puzzle.opponent = [GWProfile createEntryForID:@2];
        self.puzzle.my = @NO;
        self.puzzle.imageURL = @"http://s21.postimg.org/jqaqeixpj/KANGA.jpg";
        [self loadImage];
    }
    
    self.answerView.help = self.puzzle.answer;
    [self.controllContainerView addSubview:self.answerView];
    [self.timerContainerView addSubview:self.timerView];
    
    self.order = [[NSArray arrayNumbersWithString:self.puzzle.order] mutableCopy];
    
    self.collectionView.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0.302 alpha:1.0].CGColor;
    self.collectionView.layer.borderWidth = 2.0;
    self.collectionView.layer.cornerRadius = 15.0;
    
    NSTimeInterval addTime = [NSUserDefaults standardUserDefaults].seconds ? 20 : 0;
    
    self.timerView.timerLabel.text = [NSString stringWithFormat:@"%@", @(TIMER_TILES_START_COUNT+addTime)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.collectionView.hidden = NO;
}

-(void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y -= size.height;
        self.controllContainerView.frame = frame;
    } completion:nil];
    
}

-(void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    NSNumber *number = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    double duration = [number doubleValue];
    
    NSInteger options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        CGRect frame = self.controllContainerView.frame;
        frame.origin.y += size.height;
        self.controllContainerView.frame = frame;
    } completion:nil];

}

-(void)imageDidLoad:(UIImage *)image
{
    self.image = image;
    self.images = [[self.image splitImage] mutableCopy];
    UIImage *imageCrop = self.images[0];
    _ratio = imageCrop.size.height/imageCrop.size.width;
    [self applyOrder];
    [self.collectionView reloadData];
    if (!self.demoVersion) {
        NSTimeInterval addTime = [NSUserDefaults standardUserDefaults].seconds ? 20 : 0;
        [self.timerView startTimer:TIMER_TILES_START_COUNT+addTime];
    }
}

- (void) applyOrder
{
    NSMutableArray *tempImages = [NSMutableArray arrayWithCapacity:9];
    for (NSNumber *index in self.order) {
        if (index.integerValue <9) {
            UIImage *image = self.images[index.integerValue];
            [tempImages addObject:image];
        }
    }
    self.images = tempImages;
}

- (void) checkOrder
{
    BOOL result = YES;
    int i = 0;
    for (NSNumber *index in self.order) {
        if (index.integerValue != i) {
            result = NO;
            break;
        } else {
            i++;
        }
    }
    if (result) {
        self.collectionView.hidden = YES;
        [self gameWin];
    }
}









- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GWTilesCell *cell = (GWTilesCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"GWTilesCell" forIndexPath:indexPath];
    
    cell.imageView.image = self.images[indexPath.row];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(99, 99);
    
    size.height *= _ratio;
    
    return size;
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [[[GWAudioPlayer alloc] initWithSound:@"TilesPop"]playIfSoundisEnabled];
    NSString *index = [self.images objectAtIndex:fromIndexPath.item];
    NSString *index2 = [self.images objectAtIndex:toIndexPath.item];
    
    [self.images removeObjectAtIndex:fromIndexPath.item];
    [self.images insertObject:index atIndex:toIndexPath.item];
    
    [self.images removeObject:index2];
    [self.images insertObject:index2 atIndex:fromIndexPath.item];
    
    
    NSString *oindex = [self.order objectAtIndex:fromIndexPath.item];
    NSString *oindex2 = [self.order objectAtIndex:toIndexPath.item];
    
    [self.order removeObjectAtIndex:fromIndexPath.item];
    [self.order insertObject:oindex atIndex:toIndexPath.item];
    
    [self.order removeObject:oindex2];
    [self.order insertObject:oindex2 atIndex:fromIndexPath.item];
    
    [self checkOrder];
    
    while (self.images.count < 9) {
        [self.images addObject:[[UIImage alloc]init]];
    }
    
    while (self.order.count < 9) {
        [self.order addObject:@0];
    }
}



@end
