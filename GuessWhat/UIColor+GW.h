//
//  UIColor+GW.h
//  Phuzzle
//
//  Created by Artem Antipov on 25.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GW)

+ (UIColor*) defaultTextColor;

@end
