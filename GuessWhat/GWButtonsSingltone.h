//
//  GWButtonsSingltone.h
//  Phuzzle
//
//  Created by Artem Antipov on 15.01.14.
//  Copyright (c) 2014 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWButtonsSingltone : NSObject

+ (GWButtonsSingltone*)sharedInstance;

@property (nonatomic, strong) NSMutableDictionary *buttonsSound;
@property (nonatomic, strong) NSMutableDictionary *buttonsMute;
@end
