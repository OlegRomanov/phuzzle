//
//  UIApplication+AppDimensions.h
//  HeartLeaf
//
//  Created by Artem Antipov on 29.10.13.
//  Copyright (c) 2013 Nicecode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (AppDimensions)

+(CGSize) currentSize;
+(CGSize) sizeInOrientation:(UIInterfaceOrientation)orientation;

@end
