//
//  NSUserDefaults+GW.h
//  Phuzzle
//
//  Created by Artem Antipov on 19.12.13.
//  Copyright (c) 2013 Artem Antipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (GW)

@property (nonatomic, readwrite) NSString *devicePuzzleType;
@property (nonatomic, readwrite) NSString *deviceToken;

@property (nonatomic, readwrite) NSNumber *currentUserID;

@property (nonatomic, readwrite) NSString *sessionToken;
@property (nonatomic, readwrite) NSString *username;
@property (nonatomic, readwrite) NSString *password;

@property (nonatomic, readwrite) BOOL facebookSession;
@property (nonatomic, readwrite) NSNumber *facebookID;

@property (nonatomic, readwrite) NSNumber *recivedPuzzleID;

@property (nonatomic, readwrite) NSDate *startTimerTime;

@property (nonatomic, readwrite) BOOL rememberSession;
@property (nonatomic, readwrite) BOOL soundStatus;

@property (nonatomic, readwrite) BOOL quizHelp;
@property (nonatomic, readwrite) BOOL spyholeHelp;

@property (nonatomic, readwrite) BOOL review;
@property (nonatomic, readwrite) BOOL seconds;

@property (nonatomic, readwrite) BOOL shouldStartGame;

- (void) logout;

@end
